<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SepCore extends Model
{
    protected $table = 'September2021';
    public $timestamps = false;
    protected $primaryKey = 'BAC_Account';
    public $incrementing = false;
}

