<?php

namespace App\Http\Controllers\Voyager;

use App\AugCore;
use App\Data;
use App\Host;
use App\JulyCore;
use App\NovCore;
use App\OctCore;
use App\SepCore;
use App\User;
use App\WinLoss;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;
use Intervention\Image\ImageManager;

class VoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerController
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function makeImageWinLoss($accountId, $winLoss)
    {
        $img = Image::make(public_path('WinLoss/WIN_LOSS_STATEMENT_BALLYS_KC.jpg'));
        $img->text($winLoss->BAC_FName . ' ' . $winLoss->BAC_LName, 205, 107, function ($font) {
            $font->file(public_path('WinLoss/font.ttf'));
            $font->size(20);
            $font->color('#000000');
            $font->align('left');
//            $font->valign('top');
            $font->angle(0);
        });
        $img->text($winLoss->BAC_FName . ',', 137, 406, function ($font) {
            $font->file(public_path('WinLoss/font.ttf'));
            $font->size(22);
            $font->color('#000000');
            $font->align('left');
//            $font->valign('top');
            $font->angle(0);
        });
        $img->text($winLoss->BAC_Address_01.' '.$winLoss->BAC_City.' '.$winLoss->BAC_State.' '.$winLoss->BAC_Postal_Code, 230, 135, function ($font) {
            $font->file(public_path('WinLoss/font.ttf'));
            $font->size(20);
            $font->color('#000000');
            $font->align('left');
//            $font->valign('top');
            $font->angle(0);
        });
        $img->text($winLoss->BAC_DTYear.'*', 685, 504, function ($font) {
            $font->file(public_path('WinLoss/fontb.ttf'));
            $font->size(21);
            $font->color('#000000');
            $font->align('left');
//            $font->valign('top');
            $font->angle(0);
        });
        $img->text('$('.$winLoss->BAC_Win_Loss.')', 367, 604, function ($font) {
            $font->file(public_path('WinLoss/fontb.ttf'));
            $font->size(21);
            $font->color('#000000');
            $font->align('left');
//            $font->valign('top');
            $font->angle(0);
        });
        if (!File::exists('WinLoss/ImageByAccountId/' . $accountId)) {
            File::makeDirectory('WinLoss/ImageByAccountId/' . $accountId, $mode = 0777, true, true);
            $img->save(public_path('WinLoss/ImageByAccountId/' . $accountId . '/' . $winLoss->BAC_DTYear . '.jpg'));
        } else {
//            if (!File::exists('WinLoss/ImageByAccountId/' . $accountId . '/' . $winLoss->BAC_DTYear . '.jpg'))
            {
                $img->save(public_path('WinLoss/ImageByAccountId/' . $accountId . '/' . $winLoss->BAC_DTYear . '.jpg'));
            }
        }
    }

    public function getDataByAccountId($accountId)
    {
        //define data result
        $data = new \stdClass();
        $data->first_name = '';
        $data->last_name = '';
        $data->BAC_Tier = '';
        $data->BAC_Reward_Points = 0;
        $data->BAC_Tier_Points = 0;
        $data->BAC_Points_Next_Tier = 0;
        $data->BAC_Account_Number = $accountId;
        $data->BAC_Player_ID = $accountId;
        $data->BAC_Address_01 = '';
        $data->BAC_Address_02 = '';
        $data->BAC_City = '';
        $data->BAC_State = '';
        $data->BAC_Zip = '';
        $data->BAC_Host = NULL;
        $data->hostData = new \stdClass();
        $data->hostData->Host_Name = NULL;
        $data->hostData->Marketing_Name = 'Guest Services';
        $data->hostData->Email = 'Email:dmincey@ballysvicksburg.com';
        $data->hostData->Phone = '601-629-2505';
        $data->hostData->Cell_Phone = NULL;
        $data->updated_at = now();
        $data->winLoss = null;
        //FLAG TO CHECK IF HAVE DATA
        $data->flgRewardsClub = 0;
        $data->flgAdditionalOffersPromotions = 0;
        $data->flgWinloss = 0;
        //COUNT TO CALC DIV
        $data->countRewardsClub = 0;
        $data->countAdditionalOffersPromotions = 0;

        //Get WLoss Data
        $data->winLoss = WinLoss::where('BAC_Account_Number', $accountId)->get();
        //check if have data
        if(count($data->winLoss)>0)
            $data->flgWinloss = 1;
        //Make Images For WinLoss
        foreach ($data->winLoss as $winLoss) {
            $this->makeImageWinLoss($accountId, $winLoss);
            $winLoss->imgLink = '/WinLoss/ImageByAccountId/' . $accountId . '/' . $winLoss->BAC_DTYear . '.jpg';
        }
        //get data from Data table
        $dataFromDataTable = Data::where('BAC_Player_ID', $accountId)->first();
        if ($dataFromDataTable) {
            $data->first_name = $dataFromDataTable->BAC_FName;
            $data->last_name = $dataFromDataTable->BAC_Lname;
            $data->BAC_Tier = $dataFromDataTable->BAC_Tier;
            $data->BAC_Reward_Points = $dataFromDataTable->BAC_Reward_Points ? $dataFromDataTable->BAC_Reward_Points : 0;
            $data->BAC_Tier_Points = $dataFromDataTable->BAC_Tier_Points ? $dataFromDataTable->BAC_Tier_Points : 0;
            $data->BAC_Points_Next_Tier = $dataFromDataTable->BAC_Points_Next_Tier ? $dataFromDataTable->BAC_Points_Next_Tier : 0;
            $data->BAC_MI = $dataFromDataTable->BAC_MI;
            $data->BAC_Host = $dataFromDataTable->BAC_Host;
            //Get Host
            $data->BAC_Host = $dataFromDataTable->BAC_Host;
            $host = Host::where('Host_Name',$data->BAC_Host)->first();
            if($host)
                $data->hostData = $host;
            $data->BAC_Host_ID = $dataFromDataTable->BAC_Host_ID;
            $data->BAC_Address_01 = $dataFromDataTable->BAC_Address_01;
            $data->BAC_Address_02 = $dataFromDataTable->BAC_Address_02;
            $data->BAC_City = $dataFromDataTable->BAC_City;
            $data->BAC_State = $dataFromDataTable->BAC_State;
            $data->BAC_Zip = $dataFromDataTable->BAC_Zip;
        }

        //Get data from NovCore table
        //Define data type flag
        $data->novCoreSM6 = 0;
        $data->novCoreSM6Label = '';

        $data->novCoreSM4 = 0;
        $data->novCoreSM4Label = '';

        $data->novCruiseCertificate = 0;
        $data->novCruiseCertificateLabel = '';

        $dataFromNov = NovCore::where('BAC_Account',$accountId)->get();
        if($dataFromNov)
        {
            foreach($dataFromNov as $singleDataFromNov)
            {
                //Check and get Rewards Club
                if($singleDataFromNov->BAC_Mailer_Type === "CORE_SM"){
                    if($singleDataFromNov->BAC_Img_Page05 === NULL)
                    {
                        $data->novCoreSM4 = 1;
                        $data->flgRewardsClub = 1;
                        $data->novCoreSM4Label = $singleDataFromNov->BAC_Label;
                        $data->countRewardsClub++;
                        $data->novCoreSM4Result1 = $singleDataFromNov->BAC_Img_Page01.".jpg";
                        $data->novCoreSM4Result2 = $singleDataFromNov->BAC_Img_Page02.".jpg";
                        $data->novCoreSM4Result3 = $singleDataFromNov->BAC_Img_Page03.".jpg";
                        $data->novCoreSM4Result4 = $singleDataFromNov->BAC_Img_Page04.".jpg";

                    } else {
                        $data->novCoreSM6 = 1;
                        $data->flgRewardsClub = 1;
                        $data->novCoreSM6Label = $singleDataFromNov->BAC_Label;
                        $data->countRewardsClub++;
                        $data->novCoreSM6Result1 = $singleDataFromNov->BAC_Img_Page01.".jpg";
                        $data->novCoreSM6Result2 = $singleDataFromNov->BAC_Img_Page02.".jpg";
                        $data->novCoreSM6Result3 = $singleDataFromNov->BAC_Img_Page03.".jpg";
                        $data->novCoreSM6Result4 = $singleDataFromNov->BAC_Img_Page04.".jpg";
                        $data->novCoreSM6Result5 = $singleDataFromNov->BAC_Img_Page05.".jpg";
                        $data->novCoreSM6Result6 = $singleDataFromNov->BAC_Img_Page06.".jpg";

                    }
                   }

                //Check and get Additional Offers & Promotions
                if($singleDataFromNov->BAC_Mailer_Type === "CRUISE_CERTIFICATE"){
                    $data->novCruiseCertificate = 1;
                    $data->flgAdditionalOffersPromotions = 1;
                    $data->novCruiseCertificateLabel = $singleDataFromNov->BAC_Label;
                    $data->flgAdditionalOffersPromotions++;
                    $data->novCruiseCertificateResult1 = $singleDataFromNov->BAC_Img_Page01.".jpg";
                    $data->novCruiseCertificateResult2 = $singleDataFromNov->BAC_Img_Page02.".jpg";
                }
            }
        }

        //Get data from OctCore table
        //Define data type flag
        $data->octCoreSM = 0;
        $data->octCoreSMLabel = '';

        $data->octGiftOfTheMonth = 0;
        $data->octGiftOfTheMonthLabel = '';

        $data->octMysteryMoney = 0;
        $data->octMysteryMoneyLabel = '';

        $data->octVipGift = 0;
        $data->octVipGiftLabel = '';

        $dataFromOct = OctCore::where('BAC_Account',$accountId)->get();
        if($dataFromOct)
        {
            foreach($dataFromOct as $singleDataFromOct)
            {
                //Check and get Rewards Club

                if($singleDataFromOct->BAC_Mailer_Type === "CORE_SM"){
                    $data->octCoreSM = 0;
//                    $data->flgRewardsClub = 1;
                    $data->octCoreSMLabel = $singleDataFromOct->BAC_Label;
//                    $data->countRewardsClub++;
                    $data->octCoreSMResult1 = $singleDataFromOct->BAC_Img_Page01.".jpg";
                    $data->octCoreSMResult2 = $singleDataFromOct->BAC_Img_Page02.".jpg";
                    $data->octCoreSMResult3 = $singleDataFromOct->BAC_Img_Page03.".jpg";
                    $data->octCoreSMResult4 = $singleDataFromOct->BAC_Img_Page04.".jpg";
                    $data->octCoreSMResult5 = $singleDataFromOct->BAC_Img_Page05.".jpg";
                    $data->octCoreSMResult6 = $singleDataFromOct->BAC_Img_Page06.".jpg";
                }

                //Check and get Additional Offers & Promotions
                if($singleDataFromOct->BAC_Mailer_Type === "GIFT_OF_THE_MONTH"){
                    $data->octGiftOfTheMonth = 0;
//                    $data->flgAdditionalOffersPromotions = 0;
                    $data->octGiftOfTheMonthLabel = $singleDataFromOct->BAC_Label;
//                    $data->flgAdditionalOffersPromotions++;
                    $data->octGiftOfTheMonthResult1 = $singleDataFromOct->BAC_Img_Page01.".jpg";
                    $data->octGiftOfTheMonthResult2 = $singleDataFromOct->BAC_Img_Page02.".jpg";
                }

                if($singleDataFromOct->BAC_Mailer_Type === "MYSTERY_MONEY"){
                    $data->octMysteryMoney = 0;
//                    $data->flgAdditionalOffersPromotions = 0;
                    $data->octMysteryMoneyLabel = $singleDataFromOct->BAC_Label;
//                    $data->flgAdditionalOffersPromotions++;
                    $data->octMysteryMoneyResult1 = $singleDataFromOct->BAC_Img_Page01.".jpg";
                    $data->octMysteryMoneyResult2 = $singleDataFromOct->BAC_Img_Page02.".jpg";
                }
            }
        }

        //Set max of Rewards Club on row
        if($data->countRewardsClub>=2)
            $data->countRewardsClub = 2;
        return $data;
    }

    // Code for normal user here
    public function index()
    {
        if (Auth::user()->role_id === 1 || Auth::user()->role_id === 4)
            return Voyager::view('voyager::index');
        else {
            //Get User from user Id
            $user = User::where('id', Auth::user()->id)->first();
            $user->Email_Verified = 'Y';
            $user->save();
            $data = $this->getDataByAccountId(Auth::user()->BAC_Player_ID);
            return view('player-dashboard')->with('data', $data);
        }
    }

    //code for superUser here
    public function getViewPlayerDashBoardByAccountId($accountId)
    {
        //get user data and function get data
        $data = $this->getDataByAccountId($accountId);
        return view('player-dashboard')->with('data', $data);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('voyager.login');
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string)$image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return "<script> parent.helpers.setImageValue('" . Voyager::image($fullFilename) . "'); </script>";
    }
}
