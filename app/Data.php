<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Data extends Model
{
    protected $table = 'Datas';
    public $timestamps = false;
    protected $primaryKey = 'BAC_Player_ID';
    public $incrementing = false;
}

