<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class NovCore extends Model
{
    protected $table = 'November2021';
    public $timestamps = false;
    protected $primaryKey = 'BAC_Account';
    public $incrementing = false;
}

