@extends('layouts.app')

@section('content')
    <div id="inner-content" class="row clearfix">
        <!--Left Sidebar -->
        <div id="sidebar1" class="sidebar large-3 medium-3 columns"
             role="complementary">
            <h3>Phone</h3>
            <p>Toll Number: 1-833-576-0830</p>
            <h3>Address</h3>
            <p>1380 Warrenton Rd<br>
                Vicksburg, MS 39180</p>
            <h3>Hours</h3>
            <p><strong>Open 24/7 with the exception of Wednesdays from 5 AM to 6
                    AM.</strong></p>
            <div id="mosCenter">
                <title>Host Box</title>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">

                <link rel="stylesheet" type="text/css"
                      href="/HeaderFooterAssets/mos.css">
                <link rel="stylesheet" type="text/css"
                      href="/HeaderFooterAssets/host.css">
                <!--  START Host -->
                <div id="hostContainer">
                    <div id="hostContainerInner">
                        <div id="host" style="display: block;">
                            <div id="hostPictureBox">
                                <div id="hostPicture">
                                    @if($data->hostData->Host_Name === NULL)
                                        <img id="hostPicImg"
                                             src="/Host/BallysVK.png">
                                    @else
                                        <img id="hostPicImg"
                                             src="/Host/{{$data->hostData->Host_Name}}.jpg">
                                    @endif
                                </div>
                            </div>
                            <div id="hostInfoBox">
                                <div
                                    id="hostName">{{$data->hostData->Marketing_Name}}</div>
                                <div id="hostEmail"><a
                                        href="mailto:{{$data->hostData->Email}}"
                                        style="color:red">{{$data->hostData->Email}}</a>
                                </div>
                                <div id="hostPhone">
                                    <div class="hostText">Office:</div>
                                    <div
                                        id="hostPhoneText">{{$data->hostData->Phone}}</div>
                                </div>
                                <div id="hostCell" style="">
                                    @if($data->hostData->Cell_Phone)
                                        <div class="hostText">Mobile:</div>
                                        <div
                                            id="hostPhoneText">{{$data->hostData->Cell_Phone}}</div>
                                    @endif
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <!-- Main -->
        <div id="main" class="large-9 medium-9 columns" role="main">
            <div class="row fullwidth default-page-content" style="padding-top:0">
                <h2 style="text-align: center;color: white;background-color:#eb1d25">
                    WELCOME {{strtoupper($data->first_name).' '.strtoupper($data->last_name)}} !</h2>
                <div class="row justify-content-center" style="background: white">
                    <div class="columns large-12">
                        <div class="row fullwidth">
                            <div class="page-introduction">
                                <div>
                                    <meta name="viewport" content="width=device-width, initial-scale=1">
                                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/mos.css">
                                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/lightbox.css">
                                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/lightbox-mos.css">
                                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/buttonBar.css">
                                    <!-- MOS Start -->
                                    <div id="mosContainer">
                                        <div id="mosBody">
                                            <!-- Log out Button -->
                                            <div id="mosCenter" style="position: absolute">
                                                <div id="mosLogout" style="padding-right:30px">
                                                    <form id="moslogoutForm" name="logoutForm" action="/logout"
                                                          method="post">
                                                        @csrf
                                                        <input id="mosLogoutButton" value="Logout" type="submit">
                                                    </form>
                                                </div>
                                                <div style="clear:both;"></div>
                                            </div>
                                            <!-- Dining and Drinks -->
                                            <div id="mosCenter">
                                                <div id="buttonsArea">
                                                    <div id="buttonBarTitle">
                                                        Reservations and Dining
                                                    </div>
                                                    <div id="buttonsBarRoom">
                                                    </div>
                                                    <div id="buttonsBar">
                                                        <div class="buttons hotel"><a
                                                                href="https://casinovb.webhotel.microsdc.us/"
                                                                class="buttonText"
                                                                onclick="window.open(this.href, 'newwindow', 'width=*, height=*'); return false;"
                                                                onkeypress="window.open(this.href, 'newwindow', 'width=*, height=*'); return false;"
                                                                style="text-decoration: none;"></a></div>
                                                        <div class="buttons smallbites"><a
                                                                href="https://ballysvicksburg.com/southern-bites/"
                                                                class="buttonText"
                                                                onclick="window.open(this.href, 'newwindow', 'width=*, height=*'); return false;"
                                                                onkeypress="window.open(this.href, 'newwindow', 'width=*, height=*'); return false;"
                                                                style="text-decoration: none;"></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- eStatement -->
                                            <div id="mosCenter">
                                                <div class="row">
                                                    <div class="columns large-4">
                                                        <!--  Start eStatement  -->
                                                        <div id="eStatementContainerInner">
                                                            <div id="pointsArea">
                                                                <div id="pointsAreaRow">
                                                                    <div id="pointsAreaRowText">Reward Points</div>
                                                                    <div
                                                                        id="pointsAreaRowValue">{{number_format($data->BAC_Reward_Points)}}</div>
                                                                    <div id="pointsAreaRowText">Current Tier</div>
                                                                    <div
                                                                        id="pointsAreaRowValue">{{$data->BAC_Tier}}</div>
                                                                    <div id="pointsAreaRowText">Tier Points</div>
                                                                    <div
                                                                        id="pointsAreaRowValue">{{number_format($data->BAC_Tier_Points)}}</div>
                                                                    <div id="pointsAreaRowText">Points to Next Tier
                                                                    </div>
                                                                    <div
                                                                        id="pointsAreaRowValue">{{number_format($data->BAC_Points_Next_Tier)}}</div>
                                                                </div>
                                                            </div>
                                                            <div style="clear:both;"></div>
                                                        </div>
                                                        <!--  END eStatement  -->
                                                    </div>
                                                    <div class="columns large-4" style="text-align: center">
                                                        @switch($data->BAC_Tier)
                                                            @case('Legend')
                                                            <img src="/Cards/{{$data->BAC_Tier}}.png"
                                                                 style="max-width: 200px">
                                                            @break
                                                            @case('Pro')
                                                            <img src="/Cards/{{$data->BAC_Tier}}.png"
                                                                 style="max-width: 200px">
                                                            @break
                                                            @case('Star')
                                                            <img src="/Cards/{{$data->BAC_Tier}}.png"
                                                                 style="max-width: 200px">
                                                            @break
                                                            @case('Superstar')
                                                            <img src="/Cards/{{$data->BAC_Tier}}.png"
                                                                 style="max-width: 200px">
                                                            @break
                                                            @default
                                                            {{--                                                            <img src="/Cards/Legend.png">--}}
                                                        @endswitch
                                                    </div>

                                                    <div class="columns large-4">
                                                        <div class="mosCustomerInfoField">
                                                            Account#:{{$data->BAC_Player_ID}}
                                                        </div>
                                                        <div class="mosCustomerInfoField">
                                                            {{$data->first_name}} {{$data->last_name}}
                                                        </div>
                                                        <div class="mosCustomerInfoField">
                                                            {{$data->BAC_Address_01}}
                                                        </div>
                                                        <div class="mosCustomerInfoField">
                                                            {{$data->BAC_City}}
                                                            , {{$data->BAC_State}}  {{$data->BAC_Zip}}
                                                        </div>
                                                        <div class="mosCustomerInfoField">
                                                        </div>
                                                        <div class="mosWinLoss">
                                                            <a href="#">WIN LOSS</a>
                                                        </div>
                                                        <div style="clear:both;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <!--REWARDS CLUB & ADDITIONAL OFFERS & PROMOTIONS -->
                                            <div id="mosCenter" style="text-align:center">
                                                <!--REWARDS CLUB title -->
                                                <div id="titleRewardsClub">
                                                    <div style="margin: 0 auto;">
                                                        <h2>BALLY REWARDS</h2>
                                                    </div>
                                                </div>
                                                <!-- REWARDS CLUB -->
                                            @if($data->flgRewardsClub)
                                                <!--Rewards club data here -->
                                                    <div class="row">
                                                        <!--novCoreSM6 6 pics-->
                                                        @if($data->novCoreSM6)
                                                            <div class="columns large-{{12/$data->countRewardsClub}}">
                                                                <meta name="viewport"
                                                                      content="width = 1050, user-scalable = yes"/>
                                                                <script type="text/javascript"
                                                                        src="{{asset('flipbook_assets/js/lightbox.min.js')}}"></script>
                                                                <style>
                                                                    table {
                                                                        border-collapse: inherit !important;
                                                                    }

                                                                    @font-face {
                                                                        font-family: myFirstFont;
                                                                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                                                    }

                                                                    @font-face {
                                                                        font-family: totalfb;
                                                                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                                                    }

                                                                    #img-magnifier-container {
                                                                        display: none;
                                                                        background: rgba(0, 0, 0, 0.8);
                                                                        border: 5px solid rgb(255, 255, 255);
                                                                        border-radius: 20px;
                                                                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                                                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                                                                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                                                        cursor: none;
                                                                        position: absolute;
                                                                        pointer-events: none;
                                                                        width: 400px;
                                                                        height: 200px;
                                                                        overflow: hidden;
                                                                        z-index: 999;
                                                                    }

                                                                    .glass {
                                                                        position: absolute;
                                                                        background-repeat: no-repeat;
                                                                        background-size: auto;
                                                                        cursor: none;
                                                                        z-index: 1000;
                                                                    }

                                                                    #toggle-zoom {
                                                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                                                        background-size: 40px;
                                                                        display: block;
                                                                        width: 40px;
                                                                        display: none;
                                                                        height: 40px;
                                                                    }

                                                                    #printer {
                                                                        float: right;
                                                                        display: block;
                                                                        width: 40px;
                                                                        height: 40px;
                                                                        margin-right: 20px;
                                                                        display: none;
                                                                    }

                                                                    #toggle-zoom.toggle-on {
                                                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                                                    }

                                                                    @media (hover: none) {
                                                                        .tool-zoom {
                                                                            display: none;
                                                                        }

                                                                        #printer {
                                                                            display: none;
                                                                        }
                                                                    }
                                                                </style>
                                                                <div class="flipbook-viewport">
                                                                    <div class="container">
                                                                        <div>
                                                                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}""
                                                                               target="blank"><img
                                                                                    id="printer"
                                                                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                                                        </div>
                                                                        <div class="tool-zoom">
                                                                            <a id="toggle-zoom"
                                                                               onclick="toggleZoom()"></a>
                                                                        </div>

                                                                        <div class="arrows">
                                                                            <div class="arrow-prev">
                                                                                <a id="prev"><img class="previous"
                                                                                                  width="20"
                                                                                                  src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                                                                  alt=""/></a>
                                                                            </div>
                                                                            <center><h5
                                                                                    style="color: red;margin-right: 35px;">
                                                                                    {{$data->novCoreSM6Label}}</h5>
                                                                            </center>
                                                                            <div class="flipbook">
                                                                                <!-- Below is where you change the images for the flipbook -->
                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result1}}"
                                                                                   data-odd="1"
                                                                                   id="page-1"
                                                                                   data-lightbox="big" class="page"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result1}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result2}}"
                                                                                   data-even="2"
                                                                                   id="page-2" data-lightbox="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result2}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result3}}"
                                                                                   data-odd="3"
                                                                                   id="page-3"
                                                                                   data-lightbox="big" class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result3}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result4}}"
                                                                                   data-even="4"
                                                                                   id="page-4" data-lightbox="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result4}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result5}}"
                                                                                   data-odd="5"
                                                                                   id="page-5"
                                                                                   data-lightbox="big" class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result5}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result6}}"
                                                                                   data-even="6"
                                                                                   id="page-6" data-lightbox="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/lo_res/{{$data->novCoreSM6Result6}}')"></a>
                                                                            </div>
                                                                            <div class="arrow-next">
                                                                                <a id="next"><img class="next"
                                                                                                  width="20"
                                                                                                  src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                                                                  alt=""/></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Below are the thumbnails to the flipbook -->
                                                                <div class="flipbook-slider-thumb">
                                                                    <div class="drag" style="margin-right: 35px;">
                                                                        <img onclick="onPageClick(1)"
                                                                             class="thumb-img left-img"
                                                                             height="{{90/$data->countRewardsClub}}"
                                                                             src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result1}}"
                                                                             alt=""/>

                                                                        <div class="space">
                                                                            <img onclick="onPageClick(2)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result2}}"
                                                                                 alt=""/>
                                                                            <img onclick="onPageClick(3)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result3}}"
                                                                                 alt=""/>
                                                                        </div>

                                                                        <div class="space">
                                                                            <img onclick="onPageClick(4)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result4}}"
                                                                                 alt=""/>
                                                                            <img onclick="onPageClick(5)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM6Result5}}"
                                                                                 alt=""/>
                                                                        </div>

                                                                        <div class="space">
                                                                            <img onclick="onPageClick(6)"
                                                                                 class="thumb-img active"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/lo_res/{{$data->novCoreSM6Result6}}"
                                                                                 alt=""/>
                                                                        </div>
                                                                    </div>

                                                                    <ul class="flipbook-slick-dots" role="tablist">
                                                                        <li onclick="onPageClick(1)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">1</a>
                                                                        </li>
                                                                        <li onclick="onPageClick(2)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">2</a>
                                                                        </li>
                                                                        <li onclick="onPageClick(3)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">3</a>
                                                                        </li>
                                                                        <li onclick="onPageClick(4)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">4</a>
                                                                        </li>
                                                                        <li onclick="onPageClick(5)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">5</a>
                                                                        </li>
                                                                        <li onclick="onPageClick(6)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">6</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div id="img-magnifier-container">
                                                                    <img id="zoomed-image-container" class="glass"
                                                                         src=""/>
                                                                </div>
                                                                <div id="log"></div>
                                                                <audio id="audio" style="display: none"
                                                                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                                                                <script type="text/javascript">
                                                                    function scaleFlipBook() {
                                                                        var imageWidth = 500 / '<?php echo $data->countRewardsClub; ?>';
                                                                        if (imageWidth === 500) imageWidth = 541;
                                                                        var imageHeight = 850 / '<?php echo $data->countRewardsClub; ?>';
                                                                        var pageHeight = 615 / '<?php echo $data->countRewardsClub; ?>';
                                                                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                                                                        $(".flipbook-viewport .container").css({
                                                                            width: 40 + pageWidth * 2 + 40 + "px",
                                                                        });

                                                                        $(".flipbook-viewport .flipbook").css({
                                                                            width: pageWidth * 2 + "px",
                                                                            height: pageHeight + "px",
                                                                        });

                                                                        $(".flipbook-viewport .flipbook").css('margin-bottom', 20);
                                                                    }

                                                                    function doResize() {
                                                                        $("html").css({
                                                                            zoom: 1
                                                                        });
                                                                        var $viewport = $(".flipbook-viewport");
                                                                        var viewHeight = $viewport.height();
                                                                        var viewWidth = $viewport.width();

                                                                        var $el = $(".flipbook-viewport .container");
                                                                        var elHeight = $el.outerHeight();
                                                                        var elWidth = $el.outerWidth();

                                                                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                                                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                                                        if (scale < 1) {
                                                                            scale *= 0.95;
                                                                        } else {
                                                                            scale = 1;
                                                                        }
                                                                    }

                                                                    function loadApp() {
                                                                        scaleFlipBook();
                                                                        var flipbook = $(".flipbook");

                                                                        // Check if the CSS was already loaded

                                                                        if (flipbook.width() == 0 || flipbook.height() == 0) {
                                                                            setTimeout(loadApp, 10);
                                                                            return;
                                                                        }

                                                                        $(".flipbook .double").scissor();

                                                                        // Create the flipbook

                                                                        $(".flipbook").turn({
                                                                            // Elevation

                                                                            elevation: 50,

                                                                            // Enable gradients

                                                                            gradients: true,

                                                                            // Auto center this flipbook

                                                                            autoCenter: true,
                                                                            when: {
                                                                                turning: function (event, page, view) {
                                                                                    var audio = document.getElementById("audio");
                                                                                    audio.play();
                                                                                },
                                                                                turned: function (e, page) {
                                                                                    //console.log('Current view: ', $(this).turn('view'));
                                                                                    var thumbs = document.getElementsByClassName("thumb-img");
                                                                                    for (var i = 0; i < thumbs.length; i++) {
                                                                                        var element = thumbs[i];
                                                                                        if (element.className.indexOf("active") !== -1) {
                                                                                            $(element).removeClass("active");
                                                                                        }
                                                                                    }

                                                                                    $(
                                                                                        document.getElementsByClassName("thumb-img")[page - 1]
                                                                                    ).addClass("active");

                                                                                    var dots = document.getElementsByClassName("dot");
                                                                                    for (var i = 0; i < dots.length; i++) {
                                                                                        var dot = dots[i];
                                                                                        if (dot.className.indexOf("dot-active") !== -1) {
                                                                                            $(dot).removeClass("dot-active");
                                                                                        }
                                                                                    }
                                                                                },
                                                                            },
                                                                        });
                                                                        doResize();
                                                                    }

                                                                    $(window).resize(function () {
                                                                        doResize();
                                                                    });
                                                                    $(window).bind("keydown", function (e) {
                                                                        if (e.keyCode == 37) $(".flipbook").turn("previous");
                                                                        else if (e.keyCode == 39) $(".flipbook").turn("next");
                                                                    });
                                                                    $("#prev").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook").turn("previous");
                                                                    });

                                                                    $("#next").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook").turn("next");
                                                                    });

                                                                    $("#prev-arrow").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook").turn("previous");
                                                                    });

                                                                    $("#next-arrow").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook").turn("next");
                                                                    });

                                                                    function onPageClick(i) {
                                                                        $(".flipbook").turn("page", i);
                                                                    }

                                                                    // Load the HTML4 version if there's not CSS transform
                                                                    yepnope({
                                                                        test: Modernizr.csstransforms,
                                                                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                                                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                                                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page.css')}}"],
                                                                        complete: loadApp,
                                                                    });

                                                                    zoomToolEnabled = false;

                                                                    function toggleZoom() {
                                                                        if (zoomToolEnabled) {
                                                                            $(".flipbook a").off("mousemove");
                                                                            $("#toggle-zoom").removeClass("toggle-on");
                                                                            $("#img-magnifier-container").hide();

                                                                            zoomToolEnabled = false;
                                                                        } else {
                                                                            $(".flipbook a").mousemove(function (event) {
                                                                                var magnifier = $("#img-magnifier-container");
                                                                                $("#img-magnifier-container").css(
                                                                                    "left",
                                                                                    event.pageX - magnifier.width() / 2
                                                                                );
                                                                                $("#img-magnifier-container").css(
                                                                                    "top",
                                                                                    event.pageY - magnifier.height() / 2
                                                                                );
                                                                                $("#img-magnifier-container").show();
                                                                                var hoveredImage = $(event.target).css("background-image");
                                                                                var bg = hoveredImage
                                                                                    .replace("url(", "")
                                                                                    .replace(")", "")
                                                                                    .replace(/\"/gi, "");
                                                                                // Find relative position of cursor in image.
                                                                                var targetPage = $(event.target);
                                                                                var targetLeft = 400 / 2; // Width of glass container/2
                                                                                var targetTop = 200 / 2; // Height of glass container/2

                                                                                var zoomedImageContainer = document.getElementById(
                                                                                    "zoomed-image-container"
                                                                                );
                                                                                var zoomedImageWidth = zoomedImageContainer.width;
                                                                                var zoomedImageHeight = zoomedImageContainer.height;

                                                                                var imgXPercent =
                                                                                    (event.pageX - $(event.target).offset().left) /
                                                                                    targetPage.width();
                                                                                targetLeft -= zoomedImageWidth * imgXPercent;
                                                                                var imgYPercent =
                                                                                    (event.pageY - $(event.target).offset().top) /
                                                                                    targetPage.height();
                                                                                targetTop -= zoomedImageHeight * imgYPercent;

                                                                                $("#img-magnifier-container .glass").attr("src", bg);
                                                                                $("#img-magnifier-container .glass").css(
                                                                                    "top",
                                                                                    "" + targetTop + "px"
                                                                                );
                                                                                $("#img-magnifier-container .glass").css(
                                                                                    "left",
                                                                                    "" + targetLeft + "px"
                                                                                );
                                                                            });

                                                                            $("#toggle-zoom").addClass("toggle-on");
                                                                            zoomToolEnabled = true;
                                                                        }
                                                                    }
                                                                </script>
                                                            </div>
                                                        @endif
                                                        <!--novCoreSM2 4 pics -->
                                                        @if($data->novCoreSM4)
                                                            <div class="columns large-{{12/$data->countRewardsClub}}">
                                                                <script type="text/javascript"
                                                                        src="{{asset('flipbook_assets/js/lightbox-second.min.js')}}"></script>
                                                                <style>
                                                                    table {
                                                                        border-collapse: inherit !important;
                                                                    }

                                                                    .lb-details {
                                                                        display: none;
                                                                    }

                                                                    @font-face {
                                                                        font-family: myFirstFont;
                                                                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                                                    }

                                                                    @font-face {
                                                                        font-family: totalfb;
                                                                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                                                    }

                                                                    .dollar-amount {
                                                                        font-family: 'totalfb';
                                                                        font-weight: 700;
                                                                        font-size: 16pt;
                                                                    }

                                                                    #img-magnifier-container-second {
                                                                        display: none;
                                                                        background: rgba(0, 0, 0, 0.8);
                                                                        border: 5px solid rgb(255, 255, 255);
                                                                        border-radius: 20px;
                                                                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                                                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                                                                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                                                        cursor: none;
                                                                        position: absolute;
                                                                        pointer-events: none;
                                                                        width: 400px;
                                                                        height: 200px;
                                                                        overflow: hidden;
                                                                        z-index: 999;
                                                                    }

                                                                    .glass {
                                                                        position: absolute;
                                                                        background-repeat: no-repeat;
                                                                        background-size: auto;
                                                                        cursor: none;
                                                                        z-index: 1000;
                                                                    }

                                                                    #toggle-zoom-second {
                                                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                                                        background-size: 40px;
                                                                        display: block;
                                                                        width: 40px;
                                                                        display: none;
                                                                        height: 40px;
                                                                    }

                                                                    #printer {
                                                                        float: right;
                                                                        display: block;
                                                                        width: 40px;
                                                                        height: 40px;
                                                                        margin-right: 20px;
                                                                        display: none;
                                                                    }

                                                                    #toggle-zoom-second.toggle-on {
                                                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                                                    }

                                                                    @media (hover: none) {
                                                                        .tool-zoom {
                                                                            display: none;
                                                                        }

                                                                        #printer {
                                                                            display: none;
                                                                        }
                                                                    }
                                                                </style>
                                                                <div class="flipbook-viewport-second">
                                                                    <div class="container">
                                                                        <div>
                                                                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}""
                                                                               target="blank"><img
                                                                                    id="printer"
                                                                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                                                        </div>
                                                                        <div class="tool-zoom">
                                                                            <a id="toggle-zoom-second"
                                                                               onclick="toggleZoom()"></a>
                                                                        </div>

                                                                        <div class="arrows">
                                                                            <div class="arrow-prev">
                                                                                <a id="prev-second"><img
                                                                                        class="previous" width="20"
                                                                                        src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                                                        alt=""/></a>
                                                                            </div>
                                                                            <center><h5
                                                                                    style="color: red;">{{$data->novCoreSM4Label}}</h5>
                                                                            </center>
                                                                            <div class="flipbook-second">

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result1}}"
                                                                                   data-odd="1"
                                                                                   id="page-1"
                                                                                   data-lightbox-second="big"
                                                                                   class="page"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result1}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result2}}"
                                                                                   data-even="2"
                                                                                   id="page-2"
                                                                                   data-lightbox-second="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result2}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result3}}"
                                                                                   data-odd="3"
                                                                                   id="page-3"
                                                                                   data-lightbox-second="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result3}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result4}}"
                                                                                   data-even="4"
                                                                                   id="page-4"
                                                                                   data-lightbox-second="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/lo_res/{{$data->novCoreSM4Result4}}')"></a>
                                                                            </div>
                                                                            <div class="arrow-next">
                                                                                <a id="next-second"><img class="next"
                                                                                                         width="20"
                                                                                                         src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                                                                         alt=""/></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="flipbook-slider-thumb-second">
                                                                    <div class="drag-second">
                                                                        <img onclick="onPageClickSecond(1)"
                                                                             class="thumb-img left-img"
                                                                             src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result1}}"
                                                                             alt=""/>

                                                                        <div class="space">
                                                                            <img onclick="onPageClickSecond(2)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result2}}"
                                                                                 alt=""/>
                                                                            <img onclick="onPageClickSecond(3)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCoreSM4Result3}}"
                                                                                 alt=""/>
                                                                        </div>

                                                                        <div class="space">
                                                                            <img onclick="onPageClickSecond(4)"
                                                                                 class="thumb-img active"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/lo_res/{{$data->novCoreSM4Result4}}"
                                                                                 alt=""/>
                                                                        </div>
                                                                    </div>

                                                                    <ul class="flipbook-slick-dots-second"
                                                                        role="tablist">
                                                                        <li onclick="onPageClickSecond(1)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">1</a>
                                                                        </li>
                                                                        <li onclick="onPageClickSecond(2)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">2</a>
                                                                        </li>
                                                                        <li onclick="onPageClickSecond(3)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">3</a>
                                                                        </li>
                                                                        <li onclick="onPageClickSecond(4)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">4</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div id="img-magnifier-container-second">
                                                                    <img id="zoomed-image-container" class="glass"
                                                                         src=""/>
                                                                </div>
                                                                <div id="log"></div>
                                                                <audio id="audio" style="display: none"
                                                                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                                                                <script type="text/javascript">
                                                                    function scaleFlipBookSecond() {
                                                                        var imageWidth = 500 / '<?php echo $data->countRewardsClub; ?>';
                                                                        if (imageWidth === 500) imageWidth = 541;
                                                                        var imageHeight = 850 / '<?php echo $data->countRewardsClub; ?>';
                                                                        var pageHeight = 615 / '<?php echo $data->countRewardsClub; ?>';
                                                                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                                                                        $(".flipbook-viewport-second .container").css({
                                                                            width: 40 + pageWidth * 2 + 40 + "px",
                                                                        });

                                                                        $(".flipbook-viewport-second .flipbook-second").css({
                                                                            width: pageWidth * 2 + "px",
                                                                            height: pageHeight + "px",
                                                                        });

                                                                        $(".flipbook-viewport-second .flipbook-second").css('margin-bottom', 20);
                                                                    }

                                                                    function doResizeSecond() {
                                                                        $("html").css({
                                                                            zoom: 1
                                                                        });
                                                                        var $viewportSecond = $(".flipbook-viewport-second");
                                                                        var viewHeight = $viewportSecond.height();
                                                                        var viewWidth = $viewportSecond.width();

                                                                        var $el = $(".flipbook-viewport-second .container");
                                                                        var elHeight = $el.outerHeight();
                                                                        var elWidth = $el.outerWidth();

                                                                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                                                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                                                        if (scale < 1) {
                                                                            scale *= 0.95;
                                                                        } else {
                                                                            scale = 1;
                                                                        }
                                                                        // $("html").css({
                                                                        //     zoom: scale
                                                                        // });
                                                                    }

                                                                    function loadAppSecond() {
                                                                        scaleFlipBookSecond();
                                                                        var flipbookSecond = $(".flipbook-second");

                                                                        // Check if the CSS was already loaded

                                                                        if (flipbookSecond.width() == 0 || flipbookSecond.height() == 0) {
                                                                            setTimeout(loadAppSecond, 10);
                                                                            return;
                                                                        }

                                                                        $(".flipbook-second .double").scissor();

                                                                        // Create the flipbook-second

                                                                        $(".flipbook-second").turn({
                                                                            // Elevation

                                                                            elevation: 50,

                                                                            // Enable gradients

                                                                            gradients: true,

                                                                            // Auto center this flipbook-second

                                                                            autoCenter: true,
                                                                            when: {
                                                                                turning: function (event, page, view) {
                                                                                    var audio = document.getElementById("audio");
                                                                                    audio.play();
                                                                                },
                                                                                turned: function (e, page) {
                                                                                    //console.log('Current view: ', $(this).turn('view'));
                                                                                    var thumbs = document.getElementsByClassName("thumb-img");
                                                                                    for (var i = 0; i < thumbs.length; i++) {
                                                                                        var element = thumbs[i];
                                                                                        if (element.className.indexOf("active") !== -1) {
                                                                                            $(element).removeClass("active");
                                                                                        }
                                                                                    }

                                                                                    $(
                                                                                        document.getElementsByClassName("thumb-img")[page - 1]
                                                                                    ).addClass("active");

                                                                                    var dots = document.getElementsByClassName("dot");
                                                                                    for (var i = 0; i < dots.length; i++) {
                                                                                        var dot = dots[i];
                                                                                        if (dot.className.indexOf("dot-active") !== -1) {
                                                                                            $(dot).removeClass("dot-active");
                                                                                        }
                                                                                    }
                                                                                },
                                                                            },
                                                                        });
                                                                        doResizeSecond();
                                                                    }

                                                                    $(window).resize(function () {
                                                                        doResizeSecond();
                                                                    });
                                                                    $(window).bind("keydown", function (e) {
                                                                        if (e.keyCode == 37) $(".flipbook-second").turn("previous");
                                                                        else if (e.keyCode == 39) $(".flipbook-second").turn("next");
                                                                    });
                                                                    $("#prev-second").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-second").turn("previous");
                                                                    });

                                                                    $("#next-second").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-second").turn("next");
                                                                    });

                                                                    $("#prev-arrow-second").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-second").turn("previous");
                                                                    });

                                                                    $("#next-arrow-second").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-second").turn("next");
                                                                    });

                                                                    function onPageClickSecond(i) {
                                                                        $(".flipbook-second").turn("page", i);
                                                                    }

                                                                    // Load the HTML4 version if there's not CSS transform
                                                                    yepnope({
                                                                        test: Modernizr.csstransforms,
                                                                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                                                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                                                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page-second.css')}}"],
                                                                        complete: loadAppSecond,
                                                                    });

                                                                    zoomToolEnabled = false;

                                                                    function toggleZoom() {
                                                                        if (zoomToolEnabled) {
                                                                            $(".flipbook-second a").off("mousemove");
                                                                            $("#toggle-zoom-second").removeClass("toggle-on");
                                                                            $("#img-magnifier-container-second").hide();

                                                                            zoomToolEnabled = false;
                                                                        } else {
                                                                            $(".flipbook-second a").mousemove(function (event) {
                                                                                var magnifier = $("#img-magnifier-container-second");
                                                                                $("#img-magnifier-container-second").css(
                                                                                    "left",
                                                                                    event.pageX - magnifier.width() / 2
                                                                                );
                                                                                $("#img-magnifier-container-second").css(
                                                                                    "top",
                                                                                    event.pageY - magnifier.height() / 2
                                                                                );
                                                                                $("#img-magnifier-container-second").show();
                                                                                var hoveredImage = $(event.target).css("background-image");
                                                                                var bg = hoveredImage
                                                                                    .replace("url(", "")
                                                                                    .replace(")", "")
                                                                                    .replace(/\"/gi, "");
                                                                                // Find relative position of cursor in image.
                                                                                var targetPage = $(event.target);
                                                                                var targetLeft = 400 / 2; // Width of glass container/2
                                                                                var targetTop = 200 / 2; // Height of glass container/2

                                                                                var zoomedImageContainer = document.getElementById(
                                                                                    "zoomed-image-container"
                                                                                );
                                                                                var zoomedImageWidth = zoomedImageContainer.width;
                                                                                var zoomedImageHeight = zoomedImageContainer.height;

                                                                                var imgXPercent =
                                                                                    (event.pageX - $(event.target).offset().left) /
                                                                                    targetPage.width();
                                                                                targetLeft -= zoomedImageWidth * imgXPercent;
                                                                                var imgYPercent =
                                                                                    (event.pageY - $(event.target).offset().top) /
                                                                                    targetPage.height();
                                                                                targetTop -= zoomedImageHeight * imgYPercent;

                                                                                $("#img-magnifier-container-second .glass").attr("src", bg);
                                                                                $("#img-magnifier-container-second .glass").css(
                                                                                    "top",
                                                                                    "" + targetTop + "px"
                                                                                );
                                                                                $("#img-magnifier-container-second .glass").css(
                                                                                    "left",
                                                                                    "" + targetLeft + "px"
                                                                                );
                                                                            });

                                                                            $("#toggle-zoom-second").addClass("toggle-on");
                                                                            zoomToolEnabled = true;
                                                                        }
                                                                    }
                                                                </script>
                                                            </div>
                                                        @endif
                                                        <!--octCoreSM 6pics-->
                                                        @if($data->octCoreSM)
                                                            <div class="columns large-{{12/$data->countRewardsClub}}">
                                                                <meta name="viewport"
                                                                      content="width = 1050, user-scalable = yes"/>
                                                                <script type="text/javascript"
                                                                        src="{{asset('flipbook_assets/js/lightbox-fifth.min.js')}}"></script>
                                                                <style>
                                                                    table {
                                                                        border-collapse: inherit !important;
                                                                    }

                                                                    @font-face {
                                                                        font-family: myFirstFont;
                                                                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                                                    }

                                                                    @font-face {
                                                                        font-family: totalfb;
                                                                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                                                    }

                                                                    #img-magnifier-container {
                                                                        display: none;
                                                                        background: rgba(0, 0, 0, 0.8);
                                                                        border: 5px solid rgb(255, 255, 255);
                                                                        border-radius: 20px;
                                                                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                                                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                                                                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                                                        cursor: none;
                                                                        position: absolute;
                                                                        pointer-events: none;
                                                                        width: 400px;
                                                                        height: 200px;
                                                                        overflow: hidden;
                                                                        z-index: 999;
                                                                    }

                                                                    .glass {
                                                                        position: absolute;
                                                                        background-repeat: no-repeat;
                                                                        background-size: auto;
                                                                        cursor: none;
                                                                        z-index: 1000;
                                                                    }

                                                                    #toggle-zoom {
                                                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                                                        background-size: 40px;
                                                                        display: block;
                                                                        width: 40px;
                                                                        display: none;
                                                                        height: 40px;
                                                                    }

                                                                    #printer {
                                                                        float: right;
                                                                        display: block;
                                                                        width: 40px;
                                                                        height: 40px;
                                                                        margin-right: 20px;
                                                                        display: none;
                                                                    }

                                                                    #toggle-zoom.toggle-on {
                                                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                                                    }

                                                                    @media (hover: none) {
                                                                        .tool-zoom {
                                                                            display: none;
                                                                        }

                                                                        #printer {
                                                                            display: none;
                                                                        }
                                                                    }
                                                                </style>
                                                                <div class="flipbook-viewport-fifth">
                                                                    <div class="container">
                                                                        <div>
                                                                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}""
                                                                               target="blank"><img
                                                                                    id="printer"
                                                                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                                                        </div>
                                                                        <div class="tool-zoom">
                                                                            <a id="toggle-zoom-fifth"
                                                                               onclick="toggleZoom()"></a>
                                                                        </div>

                                                                        <div class="arrows">
                                                                            <div class="arrow-prev">
                                                                                <a id="prev-fifth"><img class="previous"
                                                                                                        width="20"
                                                                                                        src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                                                                        alt=""/></a>
                                                                            </div>
                                                                            <center><h5
                                                                                    style="color: red;margin-right: 35px;">
                                                                                    {{$data->octCoreSMLabel}}</h5>
                                                                            </center>
                                                                            <div class="flipbook-fifth">
                                                                                <!-- Below is where you change the images for the flipbook -->
                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult1}}"
                                                                                   data-odd="1"
                                                                                   id="page-1"
                                                                                   data-lightbox-fifth="big"
                                                                                   class="page"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult1}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult2}}"
                                                                                   data-even="2"
                                                                                   id="page-2" data-lightbox-fifth="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult2}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult3}}"
                                                                                   data-odd="3"
                                                                                   id="page-3"
                                                                                   data-lightbox-fifth="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult3}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult4}}"
                                                                                   data-even="4"
                                                                                   id="page-4" data-lightbox-fifth="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult4}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult5}}"
                                                                                   data-odd="5"
                                                                                   id="page-5"
                                                                                   data-lightbox-fifth="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult5}}')"></a>

                                                                                <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult6}}"
                                                                                   data-even="6"
                                                                                   id="page-6" data-lightbox-fifth="big"
                                                                                   class="single"
                                                                                   style="background-image: url('https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/lo_res/{{$data->octCoreSMResult6}}')"></a>
                                                                            </div>
                                                                            <div class="arrow-next">
                                                                                <a id="next-fifth"><img class="next"
                                                                                                        width="20"
                                                                                                        src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                                                                        alt=""/></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Below are the thumbnails to the flipbook -->
                                                                <div class="flipbook-slider-thumb">
                                                                    <div class="drag" style="margin-right: 35px;">
                                                                        <!-- <img id="prev-arrow" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                                                                        <img onclick="onPageClickfifth(1)"
                                                                             class="thumb-img left-img"
                                                                             height="{{90/$data->countRewardsClub}}"
                                                                             src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult1}}"
                                                                             alt=""/>

                                                                        <div class="space">
                                                                            <img onclick="onPageClickfifth(2)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult2}}"
                                                                                 alt=""/>
                                                                            <img onclick="onPageClickfifth(3)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult3}}"
                                                                                 alt=""/>
                                                                        </div>

                                                                        <div class="space">
                                                                            <img onclick="onPageClickfifth(4)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult4}}"
                                                                                 alt=""/>
                                                                            <img onclick="onPageClickfifth(5)"
                                                                                 class="thumb-img"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octCoreSMResult5}}"
                                                                                 alt=""/>
                                                                        </div>

                                                                        <div class="space">
                                                                            <img onclick="onPageClickfifth(6)"
                                                                                 class="thumb-img active"
                                                                                 src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/lo_res/{{$data->octCoreSMResult6}}"
                                                                                 alt=""/>
                                                                        </div>
                                                                    </div>

                                                                    <ul class="flipbook-slick-dots" role="tablist">
                                                                        <li onclick="onPageClickfifth(1)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">1</a>
                                                                        </li>
                                                                        <li onclick="onPageClickfifth(2)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">2</a>
                                                                        </li>
                                                                        <li onclick="onPageClickfifth(3)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">3</a>
                                                                        </li>
                                                                        <li onclick="onPageClickfifth(4)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">4</a>
                                                                        </li>
                                                                        <li onclick="onPageClickfifth(5)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">5</a>
                                                                        </li>
                                                                        <li onclick="onPageClickfifth(6)" class="dot">
                                                                            <a type="button"
                                                                               style="color: #7f7f7f">6</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div id="img-magnifier-container">
                                                                    <img id="zoomed-image-container" class="glass"
                                                                         src=""/>
                                                                </div>
                                                                <div id="log"></div>
                                                                <audio id="audio" style="display: none"
                                                                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                                                                <script type="text/javascript">
                                                                    function scaleFlipBookfifth() {
                                                                        var imageWidth = 500 / '<?php echo $data->countRewardsClub; ?>';
                                                                        if (imageWidth === 500) imageWidth = 541;
                                                                        var imageHeight = 850 / '<?php echo $data->countRewardsClub; ?>';
                                                                        var pageHeight = 615 / '<?php echo $data->countRewardsClub; ?>';
                                                                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);

                                                                        $(".flipbook-viewport-fifth .container").css({
                                                                            width: 40 + pageWidth * 2 + 40 + "px",
                                                                        });

                                                                        $(".flipbook-viewport-fifth .flipbook-fifth").css({
                                                                            width: pageWidth * 2 + "px",
                                                                            height: pageHeight + "px",
                                                                        });

                                                                        $(".flipbook-viewport-fifth .flipbook-fifth").css('margin-bottom', 20);
                                                                    }

                                                                    function doResizefifth() {
                                                                        $("html").css({
                                                                            zoom: 1
                                                                        });
                                                                        var $viewport = $(".flipbook-viewport-fifth");
                                                                        var viewHeight = $viewport.height();
                                                                        var viewWidth = $viewport.width();

                                                                        var $el = $(".flipbook-viewport-fifth .container");
                                                                        var elHeight = $el.outerHeight();
                                                                        var elWidth = $el.outerWidth();

                                                                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                                                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                                                        if (scale < 1) {
                                                                            scale *= 0.95;
                                                                        } else {
                                                                            scale = 1;
                                                                        }
                                                                    }

                                                                    function loadAppfifth() {
                                                                        scaleFlipBookfifth();
                                                                        var flipbookfifth = $(".flipbook-fifth");

                                                                        // Check if the CSS was already loaded

                                                                        if (flipbookfifth.width() == 0 || flipbookfifth.height() == 0) {
                                                                            setTimeout(loadApp, 10);
                                                                            return;
                                                                        }

                                                                        $(".flipbook-fifth .double").scissor();

                                                                        // Create the flipbook

                                                                        $(".flipbook-fifth").turn({
                                                                            // Elevation

                                                                            elevation: 50,

                                                                            // Enable gradients

                                                                            gradients: true,

                                                                            // Auto center this flipbook

                                                                            autoCenter: true,
                                                                            when: {
                                                                                turning: function (event, page, view) {
                                                                                    var audio = document.getElementById("audio");
                                                                                    audio.play();
                                                                                },
                                                                                turned: function (e, page) {
                                                                                    //console.log('Current view: ', $(this).turn('view'));
                                                                                    var thumbs = document.getElementsByClassName("thumb-img");
                                                                                    for (var i = 0; i < thumbs.length; i++) {
                                                                                        var element = thumbs[i];
                                                                                        if (element.className.indexOf("active") !== -1) {
                                                                                            $(element).removeClass("active");
                                                                                        }
                                                                                    }

                                                                                    $(
                                                                                        document.getElementsByClassName("thumb-img")[page - 1]
                                                                                    ).addClass("active");

                                                                                    var dots = document.getElementsByClassName("dot");
                                                                                    for (var i = 0; i < dots.length; i++) {
                                                                                        var dot = dots[i];
                                                                                        if (dot.className.indexOf("dot-active") !== -1) {
                                                                                            $(dot).removeClass("dot-active");
                                                                                        }
                                                                                    }
                                                                                },
                                                                            },
                                                                        });
                                                                        doResizefifth();
                                                                    }

                                                                    $(window).resize(function () {
                                                                        doResizefifth();
                                                                    });
                                                                    $(window).bind("keydown", function (e) {
                                                                        if (e.keyCode == 37) $(".flipbook-fifth").turn("previous");
                                                                        else if (e.keyCode == 39) $(".flipbook-fifth").turn("next");
                                                                    });
                                                                    $("#prev-fifth").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-fifth").turn("previous");
                                                                    });

                                                                    $("#next-fifth").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-fifth").turn("next");
                                                                    });

                                                                    $("#prev-arrow-fifth").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-fifth").turn("previous");
                                                                    });

                                                                    $("#next-arrow-fifth").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".flipbook-fifth").turn("next");
                                                                    });

                                                                    function onPageClickfifth(i) {
                                                                        $(".flipbook-fifth").turn("page", i);
                                                                    }

                                                                    // Load the HTML4 version if there's not CSS transform
                                                                    yepnope({
                                                                        test: Modernizr.csstransforms,
                                                                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                                                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                                                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page-fifth.css')}}"],
                                                                        complete: loadAppfifth,
                                                                    });

                                                                    zoomToolEnabled = false;

                                                                    function toggleZoom() {
                                                                        if (zoomToolEnabled) {
                                                                            $(".flipbook-fifth a").off("mousemove");
                                                                            $("#toggle-zoom").removeClass("toggle-on");
                                                                            $("#img-magnifier-container-fifth").hide();

                                                                            zoomToolEnabled = false;
                                                                        } else {
                                                                            $(".flipbook-fifth a").mousemove(function (event) {
                                                                                var magnifier = $("#img-magnifier-container-fifth");
                                                                                $("#img-magnifier-container-fifth").css(
                                                                                    "left",
                                                                                    event.pageX - magnifier.width() / 2
                                                                                );
                                                                                $("#img-magnifier-container-fifth").css(
                                                                                    "top",
                                                                                    event.pageY - magnifier.height() / 2
                                                                                );
                                                                                $("#img-magnifier-container-fifth").show();
                                                                                var hoveredImage = $(event.target).css("background-image");
                                                                                var bg = hoveredImage
                                                                                    .replace("url(", "")
                                                                                    .replace(")", "")
                                                                                    .replace(/\"/gi, "");
                                                                                // Find relative position of cursor in image.
                                                                                var targetPage = $(event.target);
                                                                                var targetLeft = 400 / 2; // Width of glass container/2
                                                                                var targetTop = 200 / 2; // Height of glass container/2

                                                                                var zoomedImageContainer = document.getElementById(
                                                                                    "zoomed-image-container"
                                                                                );
                                                                                var zoomedImageWidth = zoomedImageContainer.width;
                                                                                var zoomedImageHeight = zoomedImageContainer.height;

                                                                                var imgXPercent =
                                                                                    (event.pageX - $(event.target).offset().left) /
                                                                                    targetPage.width();
                                                                                targetLeft -= zoomedImageWidth * imgXPercent;
                                                                                var imgYPercent =
                                                                                    (event.pageY - $(event.target).offset().top) /
                                                                                    targetPage.height();
                                                                                targetTop -= zoomedImageHeight * imgYPercent;

                                                                                $("#img-magnifier-container-fifth .glass").attr("src", bg);
                                                                                $("#img-magnifier-container-fifth .glass").css(
                                                                                    "top",
                                                                                    "" + targetTop + "px"
                                                                                );
                                                                                $("#img-magnifier-container-fifth .glass").css(
                                                                                    "left",
                                                                                    "" + targetLeft + "px"
                                                                                );
                                                                            });

                                                                            $("#toggle-zoom").addClass("toggle-on");
                                                                            zoomToolEnabled = true;
                                                                        }
                                                                    }
                                                                </script>
                                                            </div>
                                                        @endif
                                                    </div>
                                                @else
                                                    <div id="mosContentLogin" class="mosCenterText" style="">
                                                        <div class="mosContentLoginTitle">Sorry, there are no offers
                                                            available at
                                                            this time
                                                        </div>
                                                        <p>
                                                            Come back and visit soon to qualify for future offers!
                                                        </p>
                                                        <br>
                                                    </div>
                                                @endif
                                                <br>
                                                <!--ADDITIONAL OFFERS & PROMOTIONS Title -->
                                                <div id="titleAdditionalOffersPromotions">
                                                    <div style="margin: 0 auto;">
                                                        <h2>ADDITIONAL OFFERS & PROMOTIONS</h2>
                                                    </div>
                                                </div>
                                                <!-- ADDITIONAL OFFERS & PROMOTIONS -->
                                                @if($data->flgAdditionalOffersPromotions)
                                                    <div class="row">
                                                        <!-- Additional Offers & Promotions data here -->
                                                    </div>
                                                    <div class="row">
                                                        <!--octGiftOfTheMonth - lightbox-fourth -->
                                                        @if($data->octGiftOfTheMonth)
                                                            <div
                                                                class="columns large-4">
                                                                <h5 style="color: red;text-align: center;">{{$data->octGiftOfTheMonthLabel}}</h5>
                                                                <body>
                                                                <div class="modal-wrap">
                                                                    <div class="slider-big">
                                                                        <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octGiftOfTheMonthResult1}}"
                                                                           data-lightbox-fourth="roadtrip"
                                                                           style="text-align: center">
                                                                            <img
                                                                                src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octGiftOfTheMonthResult1}}"
                                                                                style="width: 75%;margin: 0 auto;max-width: 120px"
                                                                                alt="">
                                                                        </a>
                                                                        <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octGiftOfTheMonthResult2}}"
                                                                           data-lightbox-fourth="roadtrip">
                                                                            <img
                                                                                src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octGiftOfTheMonthResult2}}"
                                                                                style="width: 100%" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <script
                                                                    src="/weekender_assets/js/modal_lightbox_fourth.min.js"></script>
                                                                </body>
                                                            </div>
                                                        @endif
                                                    <!--octGiftOfTheMonth - lightbox-fourth disabled -->
                                                        @if($data->octMysteryMoney && 1 === 0)
                                                            <div
                                                                class="columns large-4">
                                                                <h5 style="color: red;text-align: center;">{{$data->octMysteryMoneyLabel}}</h5>
                                                                <body>
                                                                <div class="modal-wrap">
                                                                    <div class="slider-big">
                                                                        <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octMysteryMoneyResult1}}"
                                                                           data-lightbox-sixth="roadtrip"
                                                                           style="text-align: center">
                                                                            <img
                                                                                src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octMysteryMoneyResult1}}"
                                                                                style="width: 75%;margin: 0 auto;max-width: 120px"
                                                                                alt="">
                                                                        </a>
                                                                        <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octMysteryMoneyResult2}}"
                                                                           data-lightbox-sixth="roadtrip">
                                                                            <img
                                                                                src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/october_2021/hi_res/{{$data->octMysteryMoneyResult2}}"
                                                                                style="width: 100%" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <script
                                                                    src="/weekender_assets/js/modal_lightbox_sixth.min.js"></script>
                                                                </body>
                                                            </div>
                                                        @endif
                                                    <!--novCruiseCertificate - lightbox-fourth -->
                                                        @if($data->novCruiseCertificate)
                                                            <div
                                                                class="columns large-4 end">
                                                                <h5 style="color: red;text-align: center;">{{$data->novCruiseCertificateLabel}}</h5>
                                                                <body>
                                                                <div class="modal-wrap">
                                                                    <div class="slider-big">
                                                                        <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCruiseCertificateResult1}}"
                                                                           data-lightbox-seventh="roadtrip"
                                                                           style="text-align: center">
                                                                            <img
                                                                                src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCruiseCertificateResult1}}"
                                                                                style="width: 75%;margin: 0 auto;max-width: 120px"
                                                                                alt="">
                                                                        </a>
                                                                        <a href="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCruiseCertificateResult2}}"
                                                                           data-lightbox-seventh="roadtrip">
                                                                            <img
                                                                                src="https://s3.us-east-1.amazonaws.com/ballysvicksburg.maplewebservices.com/november_2021/hi_res/{{$data->novCruiseCertificateResult2}}"
                                                                                style="width: 100%" alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <script
                                                                    src="/weekender_assets/js/modal_lightbox_seventh.min.js"></script>
                                                                </body>
                                                            </div>
                                                        @endif
                                                    </div>
                                                @else
                                                    <div id="mosContentLogin" class="mosCenterText" style="">
                                                        <div class="mosContentLoginTitle">Sorry, there are no offers
                                                            available at
                                                            this time
                                                        </div>
                                                        <p>
                                                            Come back and visit soon to qualify for future offers!
                                                        </p>
                                                        <br>
                                                    </div>
                                                @endif
                                                <br>
                                                <!--  Start WinLoss -->
                                                <div id="promosWLoss" style="display: none">
                                                    <div id="titleWinLoss">
                                                        <div style="margin: 0 auto;">
                                                            <h2>W2G / Win Loss</h2>
                                                        </div>
                                                    </div>
                                                    @if($data->flgWinloss)
                                                        <div class="row">
                                                            <div id="promoGridBoxesWLoss" style="position: relative;">
                                                                <!--  Promos Start here -->
                                                                <div class="row">
                                                                    @foreach($data->winLoss as $winLoss)
                                                                        <div class="columns large-4">
                                                                            <div
                                                                                class="promoGridBoxTextDisplayWLoss"
                                                                            >
                                                                                Win Loss Report {{$winLoss->BAC_DTYear}}
                                                                            </div>
                                                                            <div class="modal-wrap">
                                                                                <div class="slider-big">
                                                                                    <a href="{{$winLoss->imgLink}}"
                                                                                       data-lightbox-{{$winLoss->BAC_DTYear}}="roadtrip"
                                                                                       style="min-width: 222px;">
                                                                                        <img
                                                                                            src="{{$winLoss->imgLink}}"
                                                                                            style="width: 100%;margin: 0 auto;"
                                                                                            alt="">
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <script
                                                                            src="/weekender_assets/js/modal_lightbox_{{$winLoss->BAC_DTYear}}.min.js"></script>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div id="promoMsgWLoss" style=" font-size: 24px;">
                                                            <p>
                                                                <span class="mosContentLoginTitle">Sorry, no Win Loss information is available.<br></span>
                                                            </p>
                                                        </div>
                                                    @endif
                                                    <div style="clear:both;"></div>

                                                </div>
                                                <script src="/weekender_assets/js/modal_slick.min.js"></script>
                                                <script src="/weekender_assets/js/modal_main.js"></script>
                                                <!--  End WinLoss -->
                                                <div id="MOS-Offers">
                                                    Powered By Maple Online Services
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/mos-CasinoKC.css">
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    (function ($) {
                        'use strict';

                        $(document).ready(function () {
                            $('.mosWinLoss').click(function () {
                                $('#promosWLoss').show();
                                $('html,body').animate({scrollTop: $("#promosWLoss").offset().top - 45});
                            });
                        });
                    })(jQuery);
                </script>
            </div>
        </div> <!-- end #main -->

    </div> <!-- end #inner-content -->
@endsection


