<!DOCTYPE html>
<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en-US"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Icons & Favicons -->
    <link rel="icon" href="https://ballysvicksburg.com/wp-content/themes/BallysVicks/favicon.png">
    <link href="https://ballysvicksburg.com/wp-content/themes/BallysVicks/assets/images/apple-icon-touch.png" rel="apple-touch-icon">
    <!--[if IE]>
    <link rel="shortcut icon" href="https://ballysvicksburg.com/wp-content/themes/BallysVicks/favicon.ico">
    <![endif]-->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage" content="https://ballysvicksburg.com/wp-content/themes/BallysVicks/assets/images/win8-tile-icon.png">
    <meta name="theme-color" content="#121212">

    <link rel="pingback" href="https://ballysvicksburg.com/xmlrpc.php">

    <meta name="robots" content="max-image-preview:large">

    <!-- This site is optimized with the Yoast SEO plugin v14.9 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>Bally's Vicksburg | Vicksburg, Mississippi</title>
    <meta name="description" content="Bally's Vicksburg, located in Vicksburg, Mississippi is a Bally's Corp property with table games, slots, entertainment, and dining!">
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
    <link rel="canonical" href="https://ballysvicksburg.com/">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Bally's Vicksburg | Vicksburg, Mississippi">
    <meta property="og:description" content="Bally's Vicksburg, located in Vicksburg, Mississippi is a Bally's Corp property with table games, slots, entertainment, and dining!">
    <meta property="og:url" content="https://ballysvicksburg.com/">
    <meta property="og:site_name" content="Bally's Vicksburg">
    <meta property="article:modified_time" content="2021-08-17T16:18:06+00:00">
    <meta name="twitter:card" content="summary">
    <script type="text/javascript" async="" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/analytics.js"></script><script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://ballysvicksburg.com/#website","url":"https://ballysvicksburg.com/","name":"Bally&#039;s Vicksburg","description":"Formerly Casino Vicksburg","potentialAction":[{"@type":"SearchAction","target":"https://ballysvicksburg.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"WebPage","@id":"https://ballysvicksburg.com/#webpage","url":"https://ballysvicksburg.com/","name":"Bally's Vicksburg | Vicksburg, Mississippi","isPartOf":{"@id":"https://ballysvicksburg.com/#website"},"datePublished":"2015-11-18T20:32:00+00:00","dateModified":"2021-08-17T16:18:06+00:00","description":"Bally's Vicksburg, located in Vicksburg, Mississippi is a Bally's Corp property with table games, slots, entertainment, and dining!","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://ballysvicksburg.com/"]}]}]}</script>
    <!-- / Yoast SEO plugin. -->


    <link rel="dns-prefetch" href="https://maxcdn.bootstrapcdn.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Bally's Vicksburg » Feed" href="https://ballysvicksburg.com/feed/">
    <link rel="alternate" type="application/rss+xml" title="Bally's Vicksburg » Comments Feed" href="https://ballysvicksburg.com/comments/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/ballysvicksburg.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.8"}};
        !function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
    </script><script src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/wp-emoji-release.js" type="text/javascript" defer="defer"></script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/style.css" type="text/css" media="all">
    <link rel="stylesheet" id="cpsh-shortcodes-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/shortcodes.css" type="text/css" media="all">
    <link rel="stylesheet" id="wpsm_ac-font-awesome-front-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/font-awesome_002.css" type="text/css" media="all">
    <link rel="stylesheet" id="wpsm_ac_bootstrap-front-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/bootstrap-front.css" type="text/css" media="all">
    <link rel="stylesheet" id="responsive-lightbox-swipebox-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/swipebox.css" type="text/css" media="all">
    <link rel="stylesheet" id="normalize-css-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/normalize.css" type="text/css" media="all">
    <link rel="stylesheet" id="foundation-css-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/foundation.css" type="text/css" media="all">
    <link rel="stylesheet" id="site-css-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/style_002.css" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/font-awesome.css" type="text/css" media="all">
    <script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/jquery.js" id="jquery-js"></script>
    <script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/jquery_003.js" id="responsive-lightbox-swipebox-js"></script>
    <script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/infinite-scroll.js" id="responsive-lightbox-infinite-scroll-js"></script>
    <script type="text/javascript" id="responsive-lightbox-js-extra">
        /* <![CDATA[ */
        var rlArgs = {"script":"swipebox","selector":"lightbox","customEvents":"","activeGalleries":"1","animation":"1","hideCloseButtonOnMobile":"0","removeBarsOnMobile":"0","hideBars":"1","hideBarsDelay":"5000","videoMaxWidth":"1080","useSVG":"1","loopAtEnd":"0","woocommerce_gallery":"0","ajaxurl":"https:\/\/ballysvicksburg.com\/wp-admin\/admin-ajax.php","nonce":"d49324fde9"};
        /* ]]> */
    </script>
    <script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/front.js" id="responsive-lightbox-js"></script>
    <link rel="https://api.w.org/" href="https://ballysvicksburg.com/wp-json/"><link rel="alternate" type="application/json" href="https://ballysvicksburg.com/wp-json/wp/v2/pages/19"><link rel="shortlink" href="https://ballysvicksburg.com/">
    <link rel="alternate" type="application/json+oembed" href="https://ballysvicksburg.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballysvicksburg.com%2F">
    <link rel="alternate" type="text/xml+oembed" href="https://ballysvicksburg.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballysvicksburg.com%2F&amp;format=xml">
    <link rel="icon" href="https://ballysvicksburg.com/wp-content/uploads/2021/08/cropped-cropped-bly_ic_2x_rgb_pos_210420-32x32.png" sizes="32x32">
    <link rel="icon" href="https://ballysvicksburg.com/wp-content/uploads/2021/08/cropped-cropped-bly_ic_2x_rgb_pos_210420-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" href="https://ballysvicksburg.com/wp-content/uploads/2021/08/cropped-cropped-bly_ic_2x_rgb_pos_210420-180x180.png">
    <meta name="msapplication-TileImage" content="https://ballysvicksburg.com/wp-content/uploads/2021/08/cropped-cropped-bly_ic_2x_rgb_pos_210420-270x270.png">

    <!-- Drop Google Analytics here -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async="" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/js"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-171434148-1');
    </script>

    <!-- end analytics -->

    <!-- Tracking Code for Careers -->
    <script async="" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/38c83740-23ac-0139-27aa-06a60fe5fe77"></script>


    <meta class="foundation-data-attribute-namespace"><meta class="foundation-mq-xxlarge"><meta class="foundation-mq-xlarge-only"><meta class="foundation-mq-xlarge"><meta class="foundation-mq-large-only"><meta class="foundation-mq-large"><meta class="foundation-mq-medium-only"><meta class="foundation-mq-medium"><meta class="foundation-mq-small-only"><meta class="foundation-mq-small"><style></style><meta class="foundation-mq-topbar"></head>

<body class="home page-template page-template-front-page page-template-front-page-php page page-id-19">
<div class="off-canvas-wrap" data-offcanvas="">
    <div class="inner-wrap">
        <div id="container">
            <header class="header" role="banner">
                <div id="inner-header" class="row clearfix">
                    <!-- This navs will be applied to the topbar, above all content
                         To see additional nav styles, visit the /parts directory -->
                    <div class="show-for-medium-up contain-to-grid">
                        <nav class="top-bar" data-topbar="">
                            <ul class="title-area">
                                <!-- Title Area -->
                                <li class="name">
                                    <a href="https://ballysvicksburg.com/" rel="nofollow"> <!-- Bally&#039;s Vicksburg</a> -->
                                        <img src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/main-logo.png" alt="Bally's Vicksburg"></a>
                                </li>
                            </ul>

                            <section class="top-bar-section right">
                                <ul id="menu-main-navigation" class="top-bar-menu right"><li class="divider"></li><li id="menu-item-1831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-1831 not-click"><a href="https://ballysvicksburg.com/casino/">Casino</a>
                                        <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/casino/">Casino</a></li>
                                            <li id="menu-item-1081" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1081"><a href="https://ballysvicksburg.com/promotions/">Promotions</a></li>
                                            <li id="menu-item-4358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4358"><a href="https://ballysvicksburg.com/cv-sportsbook/">VB Sportsbook</a></li>
                                            <li id="menu-item-5004" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5004"><a href="https://ballysvicksburg.com/entertainment-2/">Entertainment</a></li>
                                            <li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="https://ballysvicksburg.com/slots/">Slots</a></li>
                                            <li id="menu-item-4429" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4429"><a href="https://ballysvicksburg.com/table-games/">Table Games</a></li>
                                            <li id="menu-item-4393" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4393"><a href="https://ballysvicksburg.com/winners/">Winners</a></li>
                                            <li id="menu-item-2669" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2669"><a href="https://ballysvicksburg.com/meet-your-hosts/">Meet Your Hosts</a></li>
                                            <li id="menu-item-4651" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4651"><a href="https://ballysvicksburg.com/casino-credit/">Casino Credit</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4480" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4480 not-click"><a href="https://ballysvicksburg.com/bally-rewards/">Bally Rewards</a>
                                        <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/bally-rewards/">Bally Rewards</a></li>
                                            <li id="menu-item-4657" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4657"><a target="_blank" rel="noopener" href="https://ballysvicksburg.maplewebservices.com/">Player Portal</a></li>
                                            <li id="menu-item-4479" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4479"><a href="https://ballysvicksburg.com/bally-rewards-rules/">Bally Rewards Rules</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4629" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4629 not-click"><a href="https://ballysvicksburg.com/dining/">Dining</a>
                                        <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/dining/">Dining</a></li>
                                            <li id="menu-item-4551" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4551"><a href="https://ballysvicksburg.com/southern-bites/">Southern Bites</a></li>
                                            <li id="menu-item-5059" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5059"><a href="https://ballysvicksburg.com/food-trucks/">Food Trucks</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4207" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4207 not-click"><a href="https://ballysvicksburg.com/hotel/">Hotel</a>
                                        <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/hotel/">Hotel</a></li>
                                            <li id="menu-item-4568" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4568"><a target="_blank" rel="noopener" href="https://casinovb.webhotel.microsdc.us/">Book Your Stay</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4136" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4136 not-click"><a href="https://ballysvicksburg.com/plan-an-event/">Events</a>
                                        <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/plan-an-event/">Events</a></li>
                                            <li id="menu-item-4173" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4173"><a href="https://ballysvicksburg.com/event-faq/">Event FAQs</a></li>
                                            <li id="menu-item-4171" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4171"><a href="https://ballysvicksburg.com/our-arena/">Our Arena</a></li>
                                            <li id="menu-item-4172" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4172"><a href="https://ballysvicksburg.com/river-room/">River Room</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4240" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4240"><a href="https://ballysvicksburg.com/careers/">Careers</a></li>
                                    <li class="divider"></li><li id="menu-item-96" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96"><a href="https://ballysvicksburg.com/contact/">Contact</a></li>
                                </ul></section></nav>
                    </div>

                    <div class="sticky fixed show-for-small">
                        <nav class="tab-bar">
                            <section class="middle tab-bar-section">
                                <a href="https://ballysvicksburg.com/" rel="nofollow"> <h1 class="title">Bally's Vicksburg</h1></a>
                            </section>
                            <section class="left-small">
                                <a href="#" class="left-off-canvas-toggle menu-icon"><span><i class="fa fab fa-bars"></i></span></a>
                            </section>
                        </nav>
                    </div>

                    <aside class="left-off-canvas-menu show-for-small-only">
                        <ul class="off-canvas-list">
                            <!-- <li><label>Navigation</label></li> -->
                            <a href="https://ballysvicksburg.com/"><span class="home-link">Home</span></a>

                            <ul id="menu-main-navigation-1" class="off-canvas-list"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-1831"><a href="https://ballysvicksburg.com/casino/">Casino</a>
                                    <ul class="left-submenu">
                                        <li class="back"><a href="#">Back</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1081"><a href="https://ballysvicksburg.com/promotions/">Promotions</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4358"><a href="https://ballysvicksburg.com/cv-sportsbook/">VB Sportsbook</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5004"><a href="https://ballysvicksburg.com/entertainment-2/">Entertainment</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="https://ballysvicksburg.com/slots/">Slots</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4429"><a href="https://ballysvicksburg.com/table-games/">Table Games</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4393"><a href="https://ballysvicksburg.com/winners/">Winners</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2669"><a href="https://ballysvicksburg.com/meet-your-hosts/">Meet Your Hosts</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4651"><a href="https://ballysvicksburg.com/casino-credit/">Casino Credit</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-4480"><a href="https://ballysvicksburg.com/bally-rewards/">Bally Rewards</a>
                                    <ul class="left-submenu">
                                        <li class="back"><a href="#">Back</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4657"><a target="_blank" rel="noopener" href="https://ballysvicksburg.maplewebservices.com/">Player Portal</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4479"><a href="https://ballysvicksburg.com/bally-rewards-rules/">Bally Rewards Rules</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-4629"><a href="https://ballysvicksburg.com/dining/">Dining</a>
                                    <ul class="left-submenu">
                                        <li class="back"><a href="#">Back</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4551"><a href="https://ballysvicksburg.com/southern-bites/">Southern Bites</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5059"><a href="https://ballysvicksburg.com/food-trucks/">Food Trucks</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-4207"><a href="https://ballysvicksburg.com/hotel/">Hotel</a>
                                    <ul class="left-submenu">
                                        <li class="back"><a href="#">Back</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4568"><a target="_blank" rel="noopener" href="https://casinovb.webhotel.microsdc.us/">Book Your Stay</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-4136"><a href="https://ballysvicksburg.com/plan-an-event/">Events</a>
                                    <ul class="left-submenu">
                                        <li class="back"><a href="#">Back</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4173"><a href="https://ballysvicksburg.com/event-faq/">Event FAQs</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4171"><a href="https://ballysvicksburg.com/our-arena/">Our Arena</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4172"><a href="https://ballysvicksburg.com/river-room/">River Room</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4240"><a href="https://ballysvicksburg.com/careers/">Careers</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96"><a href="https://ballysvicksburg.com/contact/">Contact</a></li>
                            </ul></ul>
                    </aside>

                    <a class="exit-off-canvas"></a>
                </div>  <!-- end #inner-header -->

            </header> <!-- end .header -->

            <div id="content">

                <div id="inner-content" class="row clearfix">

                    <div class="large-12 medium-12 columns">
                        <div class="row clearfix fullwidth slider">
                            <div aria-live="polite" id="soliloquy-container-20" class="soliloquy-container soliloquy-transition-fade  soliloquy-theme-base" style="max-width: 1185px; max-height: 545px; height: auto; background-image: none;"><div class="soliloquy-wrapper" style="max-width: 100%;"><div class="soliloquy-viewport" style="width: 100%; position: relative; height: 524px;"><ul id="soliloquy-20" class="soliloquy-slider soliloquy-slides soliloquy-wrap soliloquy-clear" style="width: auto; position: relative;"><li aria-hidden="false" class="soliloquy-item soliloquy-item-1 soliloquy-image-slide soliloquy-active-slide" draggable="false" style="list-style: outside none none; float: left; position: relative; width: 100%; z-index: 50; display: block; margin-right: -100%;"><img id="soliloquy-image-5256" class="soliloquy-image soliloquy-image-1" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/BH_Ballyverse-Slider-1185x545_c.jpeg" alt="WELCOME TO THE BALLYVERSE"></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-2 soliloquy-image-slide" draggable="false" style="list-style: outside none none; float: left; position: relative; width: 100%; z-index: 0; margin-right: -100%; display: none;"><a href="https://ballysvicksburg.com/southern-bites/" class="soliloquy-link" title="SouthernBites_Slider"><img id="soliloquy-image-4549" class="soliloquy-image soliloquy-image-2" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/SouthernBites_Slider-1185x545_c.jpg" alt="Southern Bites Now Open Daily"></a></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-3 soliloquy-image-slide" draggable="false" style="list-style: outside none none; float: left; position: relative; width: 100%; z-index: 0; margin-right: -100%; display: none;"><a href="https://ballysvicksburg.com/entertainment-2/" class="soliloquy-link" title="JR Blu"><img id="soliloquy-image-5197" class="soliloquy-image soliloquy-image-3" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/JR-Blu-1-1185x545_c.jpg" alt="JR Blu"></a></li></ul></div><div class="soliloquy-controls soliloquy-has-pager soliloquy-has-controls-direction" style="opacity: 1;"><div class="soliloquy-pager soliloquy-default-pager"><div class="soliloquy-pager-item"><a href="" data-slide-index="0" class="soliloquy-pager-link active" tabindex="0"><span>1</span></a></div><div class="soliloquy-pager-item"><a href="" data-slide-index="1" class="soliloquy-pager-link" tabindex="0"><span>2</span></a></div><div class="soliloquy-pager-item"><a href="" data-slide-index="2" class="soliloquy-pager-link" tabindex="0"><span>3</span></a></div></div><div class="soliloquy-controls-direction" aria-label="carousel buttons" aria-controls="soliloquy-container-20"><a class="soliloquy-prev" href="" tabindex="0" aria-label="previous"><span></span></a><a class="soliloquy-next" href="" tabindex="0" aria-label="next"><span></span></a></div></div></div></div><noscript><style type="text/css">#soliloquy-container-20{opacity:1}</style></noscript>						</div>
                    </div>

                    <div id="main" class="large-12 medium-12 columns frontpage-content" role="main">
                        <h1 style="text-transform: uppercase; font-family: national2bold;">Welcome to the new Bally’s Vicksburg – formerly Casino Vicksburg</h1>
                        <p>&nbsp;</p>
                        <h3 style="text-align: left;">Bally Rewards</h3>
                        <p style="text-align: left;">Become a Bally Rewards Member and earn
                            Bally Bucks using your Bally Rewards Card while playing any of your
                            favorite slot machines at Bally’s Vicksburg. Simply insert your card,
                            play your favorite game, and get rewarded!</p>
                        <p><a style="line-height: 1.5; text-decoration: none; font-weight: bold;" href="https://ballysvicksburg.com/bally-rewards/">Become a member</a> – it’s easy and free!</p>
                    </div> <!-- end #main -->

                    <div class="large-12 medium-12 columns frontpage-content-links">
                        <div class="row clearfix">
                            <h2>Quick Links</h2>
                            <a href="https://ballysvicksburg.com/cv-sportsbook/">
                                <div class="large-3 medium-3 small-6 columns quick-links">
                                    <img src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/Sportsbook-small-web-image-1.jpg" alt="">
                                    <h3 class="link-title">VB Sportsbook </h3>
                                    <div class="mask">
                                        VB Sportsbook 									</div>
                                </div>
                            </a>
                            <a href="https://ballysvicksburg.com/slots/">
                                <div class="large-3 medium-3 small-6 columns quick-links">
                                    <img src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/Slots__HomeBox.jpg" alt="">
                                    <h3 class="link-title">Slots</h3>
                                    <div class="mask">
                                        Slots									</div>
                                </div>
                            </a>
                            <a href="https://ballysvicksburg.com/dining/">
                                <div class="large-3 medium-3 small-6 columns quick-links">
                                    <img src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/dining-1.jpg" alt="">
                                    <h3 class="link-title">Dining</h3>
                                    <div class="mask">
                                        Dining									</div>
                                </div>
                            </a>
                            <a href="https://ballysvicksburg.com/promotions/">
                                <div class="large-3 medium-3 small-6 columns quick-links">
                                    <img src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/CV_QuickLink-Promos-e1593551497875.jpg" alt="">
                                    <h3 class="link-title">Promotions</h3>
                                    <div class="mask">
                                        Promotions									</div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div> <!-- end #inner-content -->

            </div> <!-- end #content -->

            <footer class="footer" role="contentinfo">
                <div id="inner-footer" class="row clearfix fullwidth">
                    <div class="large-12 medium-12 columns">
                        <div id="execphp-2" class="widget-odd widget-first widget-1 join-our-mailing-list-widget widget widget_execphp">			<div class="execphpwidget"><p style="color: #ffffff;">1380 Warrenton Rd, Vicksburg, MS 39180<br>
                                    1-833-576-0830</p>

                                <div class="center">
                                    <a href="https://twitter.com/BallysVicksburg"><span class="fa fa-twitter-square" title="Twitter" target="_blank"></span><span class="screen-reader-text">Follow Us on Twitter</span></a>

                                    <a href="https://www.facebook.com/BallysVicksburg/"><span class="fa fa-facebook-square" title="Facebook" target="_blank"></span><span class="screen-reader-text">Like Us on Facebook</span></a>

                                    <a href="https://www.instagram.com/Ballys_Vicksburg/"><span class="fa fa-instagram" title="instagram" target="_blank"></span><span class="screen-reader-text">Follow Us on Instagram</span></a>


                                    <style>a span.screen-reader-text {
                                            clip: rect(1px, 1px, 1px, 1px);
                                            position: absolute !important;
                                            height: 1px;
                                            width: 1px;
                                            overflow: hidden;
                                            color:#fff;
                                            font-size:20px;
                                        }</style>

                                </div></div>
                        </div><div id="nav_menu-2" class="widget-even widget-2 favorite-links-footer-widget widget widget_nav_menu"><h2 class="widgettitle">FAVORITE LINKS</h2><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-4635" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4635"><a href="https://ballysvicksburg.com/dining/">Dining</a></li>
                                    <li id="menu-item-150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150"><a href="https://ballysvicksburg.com/promotions/">Promotions</a></li>
                                    <li id="menu-item-174" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-174"><a href="https://ballysvicksburg.com/slots/">Slots</a></li>
                                    <li id="menu-item-4424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4424"><a href="https://ballysvicksburg.com/table-games/">Table Games</a></li>
                                    <li id="menu-item-4398" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4398"><a href="https://ballysvicksburg.com/winners/">Winners</a></li>
                                    <li id="menu-item-2733" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2733"><a href="https://ballysvicksburg.com/plan-an-event/">Plan an Event</a></li>
                                    <li id="menu-item-4360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4360"><a href="https://ballysvicksburg.com/cv-sportsbook/">VB Sportsbook</a></li>
                                    <li id="menu-item-4182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4182"><a href="https://ballysvicksburg.com/event-faq/">Event FAQs</a></li>
                                    <li id="menu-item-4493" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4493"><a href="https://ballysvicksburg.com/bally-rewards/">Bally Rewards</a></li>
                                    <li id="menu-item-2377" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2377"><a href="https://investors.twinriverwwholdings.com/home/default.aspx">Investors</a></li>
                                    <li id="menu-item-5002" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5002"><a href="https://ballysvicksburg.com/entertainment-2/">Entertainment</a></li>
                                    <li id="menu-item-5079" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5079"><a href="https://ballysvicksburg.com/careers/">Careers</a></li>
                                </ul></div></div><div id="execphp-3" class="widget-odd widget-last widget-3 widget widget_execphp">			<div class="execphpwidget"><a href="https://www.ballys.com/casinos-and-resorts.htm" target="_blank" rel="noopener"><img src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/bly_lg_1cp_wht_rev_210420.png" alt="Bally's Corporation" width="60%"> </a></div>
                        </div>								<div style="clear: both"></div>
                        <div id="execphp-4" class="widget-odd widget-first widget-1 post-footer widget_execphp center">			<div class="execphpwidget"><p style="font-weight: 300;">Copyright ©2021 Bally’s Corporation. Bally's Vicksburg is a registered trademark of Bally’s Corporation. Must be 21 or older.<br>Gambling problem? Call <a href="tel:1-888-777-9696">1-888-777-9696</a> | <a href="https://ballysvicksburg.com/privacy-policy/">Privacy Policy</a> | <a href="https://ballysvicksburg.com/cookie-policy/">Cookie Policy</a></p></div>
                        </div><div id="execphp-12" class="widget-even widget-last widget-2 widget_execphp center">			<div class="execphpwidget"><hr>

                                <p style="font-weight: 300; font-size: 0.75rem;">An inherent risk of
                                    exposure to COVID-19 exists in any public place where people are
                                    present. COVID-19 is an extremely contagious disease that can lead to
                                    severe illness and even death. According to the <a style="font-weight: 300; font-size: 0.75rem;" href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html">Centers for Disease Control and Prevention,</a>
                                    senior citizens and guests with underlying medical conditions are
                                    especially vulnerable. By visiting Tropicana Evansville you voluntarily
                                    assume all risks related to exposure to COVID-19.</p></div>
                        </div>
                    </div>
                </div> <!-- end #inner-footer -->
            </footer> <!-- end .footer -->
        </div> <!-- end #container -->
    </div> <!-- end .inner-wrap -->
</div> <!-- end .off-canvas-wrap -->
<link rel="stylesheet" id="soliloquy-lite-style-css" href="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/soliloquy.css" type="text/css" media="all">
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/bootstrap.js" id="wpsm_ac_bootstrap-js-front-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/accordion.js" id="call_ac-js-front-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/modernizr.js" id="modernizr-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/foundation.js" id="foundation-js-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/scripts.js" id="site-js-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/jquery_002.js" id="bx-slider-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/isotope.js" id="isotope-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/wp-embed.js" id="wp-embed-js"></script>
<script type="text/javascript" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/soliloquy-min.js" id="soliloquy-lite-script-js"></script>
<script type="text/javascript">
    if ( typeof soliloquy_slider === 'undefined' || false === soliloquy_slider ) {soliloquy_slider = {};}jQuery('#soliloquy-container-20').css('height', Math.round(jQuery('#soliloquy-container-20').width()/(1185/545)));jQuery(window).load(function(){var $ = jQuery;var soliloquy_container_20 = $('#soliloquy-container-20'),soliloquy_20 = $('#soliloquy-20');soliloquy_slider['20'] = soliloquy_20.soliloquy({slideSelector: '.soliloquy-item',speed: 650,pause: 6000,auto: 1,useCSS: 0,keyboard: true,adaptiveHeight: 1,adaptiveHeightSpeed: 400,infiniteLoop: 1,mode: 'fade',pager: 1,controls: 1,nextText: '',prevText: '',startText: '',stopText: '',onSliderLoad: function(currentIndex){soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden','true');soliloquy_container_20.css({'height':'auto','background-image':'none'});if ( soliloquy_container_20.find('.soliloquy-slider li').length > 1 ) {soliloquy_container_20.find('.soliloquy-controls').fadeTo(300, 1);}soliloquy_20.find('.soliloquy-item:not(.soliloquy-clone):eq(' + currentIndex + ')').addClass('soliloquy-active-slide').attr('aria-hidden','false');soliloquy_container_20.find('.soliloquy-clone').find('*').removeAttr('id');soliloquy_container_20.find('.soliloquy-controls-direction').attr('aria-label','carousel buttons').attr('aria-controls', 'soliloquy-container-20');soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-prev').attr('aria-label','previous');soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-next').attr('aria-label','next');},onSlideBefore: function(element, oldIndex, newIndex){soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden','true');$(element).addClass('soliloquy-active-slide').attr('aria-hidden','false');},onSlideAfter: function(element, oldIndex, newIndex){},});});			</script>

<script async="" src="Bally's%20Vicksburg%20|%20Vicksburg,%20Mississippi_files/p"></script></body></html>
<!-- end page -->
