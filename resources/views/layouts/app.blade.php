`<!DOCTYPE html>
<html
    class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"
    lang="en-US">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Google Fonts Below-->
    <link href="/HeaderFooterAssets/css2.css" rel="stylesheet">
    <!-- Icons & Favicons -->
    <link rel="icon" href="https://ballysvicksburg.com/wp-content/themes/BallysVicks/favicon.png">
    <link href="https://ballysvicksburg.com/wp-content/themes/BallysVicks/assets/images/apple-icon-touch.png" rel="apple-touch-icon">
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="theme-color" content="#121212">

    <link rel="pingback" href="https://ballysvicksburg.com/xmlrpc.php">
    <title>Bally's Vicksburg | Vicksburg, Mississippi</title>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
    <link rel="canonical" href="https://ballysvicksburg.com/promotions/">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Promotions - Casino Vicksburg">
    <meta property="og:url" content="https://ballysvicksburg.com/promotions/">
    <meta property="og:site_name" content="Casino Vicksburg">
    <meta property="article:modified_time" content="2020-09-25T01:36:27+00:00">
    <meta name="twitter:card" content="summary">
    <script type="application/ld+json" class="yoast-schema-graph">
        {"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://ballysvicksburg.com/#website","url":"https://ballysvicksburg.com/","name":"Casino Vicksburg","description":"-A Twin River Property","potentialAction":[{"@type":"SearchAction","target":"https://ballysvicksburg.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"WebPage","@id":"https://ballysvicksburg.com/promotions/#webpage","url":"https://ballysvicksburg.com/promotions/","name":"Promotions - Casino Vicksburg","isPartOf":{"@id":"https://ballysvicksburg.com/#website"},"datePublished":"2015-11-19T20:23:03+00:00","dateModified":"2020-09-25T01:36:27+00:00","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://ballysvicksburg.com/promotions/"]}]}]}





    </script>
    <!-- / Yoast SEO plugin. -->

    <link rel="stylesheet" id="wp-block-library-css" href="/flipbook_assets/css/double-page.css"
          type="text/css" media="all">
    <link rel="dns-prefetch" href="https://maxcdn.bootstrapcdn.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Casino Vicksburg » Feed" href="https://ballysvicksburg.com/feed/">
    <link rel="alternate" type="application/rss+xml" title="Casino Vicksburg » Comments Feed"
          href="https://ballysvicksburg.com/comments/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "/HeaderFooterAssets/wp-emoji-release.min.js?ver=5.4.2"}
        };
        /*! This file is auto-generated */
        !function (e, a, t) {
            var r, n, o, i, p = a.createElement("canvas"), s = p.getContext && p.getContext("2d");

            function c(e, t) {
                var a = String.fromCharCode;
                s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0);
                var r = p.toDataURL();
                return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()
            }

            function l(e) {
                if (!s || !s.fillText) return !1;
                switch (s.textBaseline = "top", s.font = "600 32px Arial", e) {
                    case"flag":
                        return !c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]));
                    case"emoji":
                        return !c([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340])
                }
                return !1
            }

            function d(e) {
                var t = a.createElement("script");
                t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
            }

            for (i = Array("flag", "emoji"), t.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, o = 0; o < i.length; o++) t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports.everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[i[o]]);
            t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function () {
                t.DOMReady = !0
            }, t.supports.everything || (n = function () {
                t.readyCallback()
            }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function () {
                "complete" === a.readyState && t.readyCallback()
            })), (r = t.source || {}).concatemoji ? d(r.concatemoji) : r.wpemoji && r.twemoji && (d(r.twemoji), d(r.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <script src="/HeaderFooterAssets/wp-emoji-release.js" type="text/javascript"
            defer="defer"></script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }

        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 999999999;
            background: url('/loading.gif') 50% 50% no-repeat rgb(255, 255, 255);
        }
    </style>
    <link rel="stylesheet" id="wp-block-library-css" href="/HeaderFooterAssets/style.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="cpsh-shortcodes-css" href="/HeaderFooterAssets/shortcodes.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="wpsm_ac-font-awesome-front-css"
          href="/HeaderFooterAssets/font-awesome.css" type="text/css" media="all">
    <link rel="stylesheet" id="wpsm_ac_bootstrap-front-css"
          href="/HeaderFooterAssets/bootstrap-front.css" type="text/css" media="all">
    <link rel="stylesheet" id="responsive-lightbox-swipebox-css"
          href="/HeaderFooterAssets/swipebox.css" type="text/css" media="all">
    <link rel="stylesheet" id="normalize-css-css" href="/HeaderFooterAssets/normalize.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="foundation-css-css" href="/HeaderFooterAssets/foundation.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="site-css-css" href="/HeaderFooterAssets/style_002.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-css" href="/HeaderFooterAssets/font-awesome_002.css"
          type="text/css" media="all">
    <script type="text/javascript" src="/HeaderFooterAssets/jquery.js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/jquery_003.js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/infinite-scroll.js"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var rlArgs = {
            "script": "swipebox",
            "selector": "lightbox",
            "customEvents": "",
            "activeGalleries": "1",
            "animation": "1",
            "hideCloseButtonOnMobile": "0",
            "removeBarsOnMobile": "0",
            "hideBars": "1",
            "hideBarsDelay": "5000",
            "videoMaxWidth": "1080",
            "useSVG": "1",
            "loopAtEnd": "0",
            "woocommerce_gallery": "0",
            "ajaxurl": "https:\/\/ballysvicksburg.com\/wp-admin\/admin-ajax.php",
            "nonce": "d4fec21b55"
        };
        /* ]]> */
    </script>
    <script type="text/javascript" src="/HeaderFooterAssets/front.js"></script>
    <link rel="https://api.w.org/" href="https://ballysvicksburg.com/wp-json/">
    <link rel="shortlink" href="https://ballysvicksburg.com/?p=73">
    <link rel="alternate" type="application/json+oembed"
          href="https://ballysvicksburg.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballysvicksburg.com%2Fpromotions%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://ballysvicksburg.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fballysvicksburg.com%2Fpromotions%2F&amp;format=xml">
    <meta name="msapplication-TileImage"
          content="./client-assets/wp-content/uploads/2020/07/cropped-VB_SocialMedia-FBCover-e1593619673181-270x270.jpg">

    <!-- Drop Google Analytics here -->


    <!-- end analytics -->

    <meta class="foundation-data-attribute-namespace">
    <meta class="foundation-mq-xxlarge">
    <meta class="foundation-mq-xlarge-only">
    <meta class="foundation-mq-xlarge">
    <meta class="foundation-mq-large-only">
    <meta class="foundation-mq-large">
    <meta class="foundation-mq-medium-only">
    <meta class="foundation-mq-medium">
    <meta class="foundation-mq-small-only">
    <meta class="foundation-mq-small">
    <style></style>
    <meta class="foundation-mq-topbar">
    <script type="text/javascript"
            src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
    <link rel="stylesheet"
          href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
    <script type="text/javascript"
            src="{{asset('flipbook_assets/js/dynamic-content.js')}}"></script>
</head>

<body class="page-template page-template-archive-promotions page-template-archive-promotions-php page page-id-73" style="line-height: 0">
<div id="flipbook-section">
    <div id="flipbook-section1">
        <div id="flipbook-section2">
            <div id="flipbook-section3">
                <div id="flipbook-section4">
                    <div id="flipbook-section5">
                        <div class="off-canvas-wrap" data-offcanvas="">
                            <div class="inner-wrap">
                                <div id="container" style="    line-height: 1.5;">
                                    <header class="header" role="banner">
                                        <div id="inner-header" class="row clearfix">
                                            <!-- This navs will be applied to the topbar, above all content
                                                 To see additional nav styles, visit the /parts directory -->
                                            <div class="show-for-medium-up contain-to-grid">
                                                <nav class="top-bar" data-topbar="">
                                                    <ul class="title-area">
                                                        <!-- Title Area -->
                                                        <li class="name">
                                                            <a href="https://ballysvicksburg.com/" rel="nofollow">
                                                                <!-- Casino Vicksburg</a> -->
                                                                <img src="/HeaderFooterAssets/main-logo.png"
                                                                     alt="casino vicksburg"></a>
                                                        </li>
                                                    </ul>

                                                    <section class="top-bar-section right">
                                                        <ul id="menu-main-navigation" class="top-bar-menu right"><li class="divider"></li><li id="menu-item-1831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-1831 not-click"><a href="https://ballysvicksburg.com/casino/">Casino</a>
                                                                <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/casino/">Casino</a></li>
                                                                    <li id="menu-item-1081" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1081"><a href="https://ballysvicksburg.com/promotions/">Promotions</a></li>
                                                                    <li id="menu-item-4358" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4358"><a href="https://ballysvicksburg.com/cv-sportsbook/">VB Sportsbook</a></li>
                                                                    <li id="menu-item-5004" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5004"><a href="https://ballysvicksburg.com/entertainment-2/">Entertainment</a></li>
                                                                    <li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="https://ballysvicksburg.com/slots/">Slots</a></li>
                                                                    <li id="menu-item-4429" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4429"><a href="https://ballysvicksburg.com/table-games/">Table Games</a></li>
                                                                    <li id="menu-item-4393" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4393"><a href="https://ballysvicksburg.com/winners/">Winners</a></li>
                                                                    <li id="menu-item-2669" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2669"><a href="https://ballysvicksburg.com/meet-your-hosts/">Meet Your Hosts</a></li>
                                                                    <li id="menu-item-4651" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4651"><a href="https://ballysvicksburg.com/casino-credit/">Casino Credit</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li><li id="menu-item-4480" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4480 not-click"><a href="https://ballysvicksburg.com/bally-rewards/">Bally Rewards</a>
                                                                <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/bally-rewards/">Bally Rewards</a></li>
                                                                    <li id="menu-item-4657" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4657"><a target="_blank" rel="noopener" href="https://ballysvicksburg.maplewebservices.com/">Player Portal</a></li>
                                                                    <li id="menu-item-4479" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4479"><a href="https://ballysvicksburg.com/bally-rewards-rules/">Bally Rewards Rules</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li><li id="menu-item-4629" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4629 not-click"><a href="https://ballysvicksburg.com/dining/">Dining</a>
                                                                <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/dining/">Dining</a></li>
                                                                    <li id="menu-item-4551" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4551"><a href="https://ballysvicksburg.com/southern-bites/">Southern Bites</a></li>
                                                                    <li id="menu-item-5059" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5059"><a href="https://ballysvicksburg.com/food-trucks/">Food Trucks</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li><li id="menu-item-4207" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4207 not-click"><a href="https://ballysvicksburg.com/hotel/">Hotel</a>
                                                                <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/hotel/">Hotel</a></li>
                                                                    <li id="menu-item-4568" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4568"><a target="_blank" rel="noopener" href="https://casinovb.webhotel.microsdc.us/">Book Your Stay</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li><li id="menu-item-4136" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4136 not-click"><a href="https://ballysvicksburg.com/plan-an-event/">Events</a>
                                                                <ul class="sub-menu dropdown"><li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="https://ballysvicksburg.com/plan-an-event/">Events</a></li>
                                                                    <li id="menu-item-4173" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4173"><a href="https://ballysvicksburg.com/event-faq/">Event FAQs</a></li>
                                                                    <li id="menu-item-4171" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4171"><a href="https://ballysvicksburg.com/our-arena/">Our Arena</a></li>
                                                                    <li id="menu-item-4172" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4172"><a href="https://ballysvicksburg.com/river-room/">River Room</a></li>
                                                                </ul>
                                                            </li>
                                                            <li class="divider"></li><li id="menu-item-4240" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4240"><a href="https://ballysvicksburg.com/careers/">Careers</a></li>
                                                            <li class="divider"></li><li id="menu-item-96" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-96"><a href="https://ballysvicksburg.com/contact/">Contact</a></li>
                                                        </ul></section></nav>
                                                </nav>
                                            </div>

                                            <div class=" sticky fixed show-for-small">
                                                <nav class="tab-bar">
                                                    <section class="middle tab-bar-section">
                                                        <h1 class="title">Casino Vicksburg</h1>
                                                    </section>
                                                    <section class="left-small">
                                                        <a href="#"
                                                           class="left-off-canvas-toggle menu-icon"><span></span></a>
                                                    </section>
                                                </nav>
                                            </div>

                                            <aside class="left-off-canvas-menu show-for-small-only">
                                                <ul class="off-canvas-list">
                                                    <!-- 		<li><label>Navigation</label></li> -->
                                                    <a href="https://ballysvicksburg.com/"><span class="home-link">Home</span></a>

                                                    <ul id="menu-main-navigation-1" class="off-canvas-list">
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children active has-submenu menu-item-1831">
                                                            <a href="https://ballysvicksburg.com/casino/">Casino</a>
                                                            <ul class="left-submenu">
                                                                <li class="back"><a href="#">Back</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-73 current_page_item active menu-item-1081">
                                                                    <a href="https://ballysvicksburg.com/promotions/"
                                                                       aria-current="page">Promotions</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4358">
                                                                    <a href="https://ballysvicksburg.com/cv-sportsbook/">CV
                                                                        Sportsbook</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79">
                                                                    <a href="https://ballysvicksburg.com/slots/">Slots</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4429">
                                                                    <a href="https://ballysvicksburg.com/table-games/">Table
                                                                        Games</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4393">
                                                                    <a href="https://ballysvicksburg.com/winners/">Winners</a>
                                                                </li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2669">
                                                                    <a href="https://ballysvicksburg.com/meet-your-hosts/">Meet
                                                                        Your Hosts</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-4480">
                                                            <a href="https://ballysvicksburg.com/rewards-club/">Rewards
                                                                Club</a>
                                                            <ul class="left-submenu">
                                                                <li class="back"><a href="#">Back</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4479">
                                                                    <a href="https://ballysvicksburg.com/rewards-club-rules/">Rewards
                                                                        Club Rules</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-1739">
                                                            <a href="https://ballysvicksburg.com/dining/">Dining</a>
                                                            <ul class="left-submenu">
                                                                <li class="back"><a href="#">Back</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4551">
                                                                    <a href="https://ballysvicksburg.com/southern-bites/">Southern
                                                                        Bites</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-4207">
                                                            <a href="https://ballysvicksburg.com/hotel/">Hotel</a>
                                                            <ul class="left-submenu">
                                                                <li class="back"><a href="#">Back</a></li>
                                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4568">
                                                                    <a target="_blank" rel="noopener noreferrer"
                                                                       href="https://casinovb.webhotel.microsdc.us/">Book
                                                                        Your Stay</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-4136">
                                                            <a href="https://ballysvicksburg.com/plan-an-event/">Events</a>
                                                            <ul class="left-submenu">
                                                                <li class="back"><a href="#">Back</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4173">
                                                                    <a href="https://ballysvicksburg.com/event-faq/">Event
                                                                        FAQs</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4171">
                                                                    <a href="https://ballysvicksburg.com/our-arena/">Our
                                                                        Arena</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4172">
                                                                    <a href="https://ballysvicksburg.com/river-room/">River
                                                                        Room</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-submenu menu-item-96">
                                                            <a href="https://ballysvicksburg.com/contact/">Contact</a>
                                                            <ul class="left-submenu">
                                                                <li class="back"><a href="#">Back</a></li>
                                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4240">
                                                                    <a href="https://ballysvicksburg.com/careers/">Careers</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </aside>

                                            <a class="exit-off-canvas"></a></div>  <!-- end #inner-header -->

                                    </header> <!-- end .header -->
                                    <div class="loader"></div>
                                    <div id="content">

                                        <div id="inner-content" class="row clearfix">
                                            @if (!Auth::User())
                                                <div id="sidebar1" class="sidebar large-3 medium-3 columns"
                                                     role="complementary">
                                                    <div class="row fullwidth sidebar-image">

                                                    </div>
                                                    <h3>Phone</h3>
                                                    <p>1-833-576-0830</p>
                                                    <h3>Address</h3>
                                                    <p>1380 Warrenton Rd<br>
                                                        Vicksburg, MS 39180</p>

                                                </div>
                                                <div id="main" class="large-9 medium-9 columns" role="main">
                                                    @else
                                                        <div id="main" class="large-12 medium-12 columns" role="main">
                                                        @endif
                                                        @yield('content')
                                                        <!--  MOS End -->
                                                        </div> <!-- end #main -->

                                                </div> <!-- end #inner-content -->

                                        </div> <!-- end #content -->

                                        <footer class="footer" role="contentinfo">
                                            <div id="inner-footer" class="row clearfix fullwidth">
                                                <div class="large-12 medium-12 columns">
                                                    <div id="execphp-2" class="widget-odd widget-first widget-1 join-our-mailing-list-widget widget widget_execphp">			<div class="execphpwidget"><p style="color: #ffffff;">1380 Warrenton Rd, Vicksburg, MS 39180<br>
                                                                1-833-576-0830</p>

                                                            <div class="center">
                                                                <a href="https://twitter.com/BallysVicksburg"><span class="fa fa-twitter-square" title="Twitter" target="_blank"></span><span class="screen-reader-text">Follow Us on Twitter</span></a>

                                                                <a href="https://www.facebook.com/BallysVicksburg/"><span class="fa fa-facebook-square" title="Facebook" target="_blank"></span><span class="screen-reader-text">Like Us on Facebook</span></a>

                                                                <a href="https://www.instagram.com/Ballys_Vicksburg/"><span class="fa fa-instagram" title="instagram" target="_blank"></span><span class="screen-reader-text">Follow Us on Instagram</span></a>


                                                                <style>a span.screen-reader-text {
                                                                        clip: rect(1px, 1px, 1px, 1px);
                                                                        position: absolute !important;
                                                                        height: 1px;
                                                                        width: 1px;
                                                                        overflow: hidden;
                                                                        color:#fff;
                                                                        font-size:20px;
                                                                    }</style>

                                                            </div></div>
                                                    </div><div id="nav_menu-2" class="widget-even widget-2 favorite-links-footer-widget widget widget_nav_menu"><h2 class="widgettitle">FAVORITE LINKS</h2><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-4635" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4635"><a href="https://ballysvicksburg.com/dining/">Dining</a></li>
                                                                <li id="menu-item-150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150"><a href="https://ballysvicksburg.com/promotions/">Promotions</a></li>
                                                                <li id="menu-item-174" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-174"><a href="https://ballysvicksburg.com/slots/">Slots</a></li>
                                                                <li id="menu-item-4424" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4424"><a href="https://ballysvicksburg.com/table-games/">Table Games</a></li>
                                                                <li id="menu-item-4398" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4398"><a href="https://ballysvicksburg.com/winners/">Winners</a></li>
                                                                <li id="menu-item-2733" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2733"><a href="https://ballysvicksburg.com/plan-an-event/">Plan an Event</a></li>
                                                                <li id="menu-item-4360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4360"><a href="https://ballysvicksburg.com/cv-sportsbook/">VB Sportsbook</a></li>
                                                                <li id="menu-item-4182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4182"><a href="https://ballysvicksburg.com/event-faq/">Event FAQs</a></li>
                                                                <li id="menu-item-4493" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4493"><a href="https://ballysvicksburg.com/bally-rewards/">Bally Rewards</a></li>
                                                                <li id="menu-item-2377" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2377"><a href="https://investors.twinriverwwholdings.com/home/default.aspx">Investors</a></li>
                                                                <li id="menu-item-5002" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5002"><a href="https://ballysvicksburg.com/entertainment-2/">Entertainment</a></li>
                                                                <li id="menu-item-5079" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5079"><a href="https://ballysvicksburg.com/careers/">Careers</a></li>
                                                            </ul></div></div><div id="execphp-3" class="widget-odd widget-last widget-3 widget widget_execphp">			<div class="execphpwidget"><a href="https://www.ballys.com/casinos-and-resorts.htm" target="_blank" rel="noopener"><img src="https://ballysblackhawk.com/wp-content/uploads/2021/07/bly_lg_1cp_wht_rev_210420.png" alt="Bally's Corporation" width="60%"> </a></div>
                                                    </div>								<div style="clear: both"></div>
                                                    <div id="execphp-4" class="widget-odd widget-first widget-1 post-footer widget_execphp center">			<div class="execphpwidget"><p style="font-weight: 300;">Copyright ©2021 Bally’s Corporation. Bally's Vicksburg is a registered trademark of Bally’s Corporation. Must be 21 or older.<br>Gambling problem? Call <a href="tel:1-888-777-9696">1-888-777-9696</a> | <a href="https://ballysvicksburg.com/privacy-policy/">Privacy Policy</a> | <a href="https://ballysvicksburg.com/cookie-policy/">Cookie Policy</a></p></div>
                                                    </div><div id="execphp-12" class="widget-even widget-last widget-2 widget_execphp center">			<div class="execphpwidget"><hr>

                                                            <p style="font-weight: 300; font-size: 0.75rem;">An inherent risk of
                                                                exposure to COVID-19 exists in any public place where people are
                                                                present. COVID-19 is an extremely contagious disease that can lead to
                                                                severe illness and even death. According to the <a style="font-weight: 300; font-size: 0.75rem;" href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html">Centers for Disease Control and Prevention,</a>
                                                                senior citizens and guests with underlying medical conditions are
                                                                especially vulnerable. By visiting Tropicana Evansville you voluntarily
                                                                assume all risks related to exposure to COVID-19.</p></div>
                                                    </div>
                                                </div>
                                            </div> <!-- end #inner-footer -->
                                        </footer> <!-- end .footer -->
                                    </div> <!-- end #container -->
                                </div> <!-- end .inner-wrap -->
                            </div> <!-- end .off-canvas-wrap -->
                            <script type="text/javascript" src="/HeaderFooterAssets/bootstrap.js"></script>
                            <script type="text/javascript" src="/HeaderFooterAssets/accordion.js"></script>
                            <script type="text/javascript" src="/HeaderFooterAssets/modernizr.js"></script>
                            <script type="text/javascript" src="/HeaderFooterAssets/foundation.js"></script>
                            <script type="text/javascript" src="/HeaderFooterAssets/jquery_002.js"></script>
                            <script type="text/javascript" src="/HeaderFooterAssets/isotope.js"></script>
                            <script type="text/javascript" src="/HeaderFooterAssets/wp-embed.js"></script>
                            <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/mos-CasinoVB.css">
                            <script>
                                $(window).load(function () {
                                    $(".loader").fadeOut("slow");
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<!-- end page -->


`
