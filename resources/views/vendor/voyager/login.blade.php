@extends('layouts.app')

@section('content')
    <div class="container" style="text-align: center; background: #fff; margin: 0 auto;">
        <div class="row justify-content-center">
            <div class="large-12 medium-12 columns">
                <div class="card-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <br>
                            <h3>{{ session()->get('message') }}</h3>
                        </div>
                    @else
                        <div id="mosContentTitle">Welcome to your Online Portal Review site</div>

                        <div id="mosContent">
                            <p>
                                After logging in with your User ID and Password
                                you can enter an Account ID to review and/or approve
                                player offers.
                            </p>
                        </div>

                        @if($errors->has('email') || $errors->has('user_name'))
                            <span
                                style="color: red">{{$errors->first('email') }} {{ $errors->first('user_name')}}</span>
                        @endif
                        <div class="large-3 medium-3 columns">
                        </div>
                        <div class="large-6 medium-6 columns" style="float:none; margin: 0 auto">
                            <form name="loginForm" action="{{ route('voyager.login') }}" method="post">
                                {{ csrf_field() }}
                                <div id="mosLoginFormLine0">
                                    <div class="form-group form-group-default" id="emailGroup">
                                        <div class="controls">
                                            <input type="text" name="user_name" id="user_name" value=""
                                                   placeholder="User Name or Email" class="form-control " required="">
                                        </div>
                                    </div>

                                    <div class="form-group form-group-default" id="passwordGroup">
                                        <div class="controls">
                                            <input type="password" name="password" placeholder="Password"
                                                   class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="form-group" id="rememberMeGroup">
                                        <div class="controls">
                                            <a href="/forget-password">Forget password!</a>
                                        </div>
                                    </div>


                                    <div class="form-group" id="rememberMeGroup">
                                        <div class="controls">
                                            <input type="checkbox" name="remember" id="remember" value="1"><label
                                                for="remember"
                                                class="remember-me-text">{{ __('voyager::generic.remember_me') }}</label>
                                        </div>
                                    </div>
                                    <div id="mosLoginButton">
                                        <input value="Login" type="submit">
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                            </form>
                        </div>
                        <div class="large-3 medium-3 columns">
                        </div>
                    @endif
                </div>
            </div>

        </div>
        <div class="row" style="padding-bottom: 30px">
            @if(!session()->has('message'))
            <div class="col-sm-6"><a href="register">Do not have an account? sign up now!</a></div>
            @endif
            <div class="col-sm-6"> Powered By Maple Web Services
            </div>
        </div>
    </div>
@endsection
