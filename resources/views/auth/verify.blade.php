@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <br>
                <div class="card" style="text-align: center">
                    <div class="card-header"><h2>Verify Your Email Address</h2></div>
                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                A fresh verification link has been sent to <p style="margin-bottom: 0"><b>{{$data->email}}</b></p>

                            </div>
                        @else
                            A fresh verification link has been sent to <p style="margin-bottom: 0"><b>{{$data->email}}</b></p>
                        Before proceeding, please check your email for a verification link.
                        @endif
                        <form action="{{ route('verification.resend') }}" method="post">
                            @csrf
                            <br>
                            <button type="submit" class="btn btn-primary" style="background-color: #59588c; border-color: #59588c; color: #fff;display: block; margin:0 auto">Request a new link</button>
                        </form>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
@endsection
